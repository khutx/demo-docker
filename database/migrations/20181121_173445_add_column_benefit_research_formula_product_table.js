'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.addColumn('products', 'benefit', {
          type: Sequelize.TEXT
        }),
        queryInterface.addColumn('products', 'research', {
          type: Sequelize.TEXT
        }),
        queryInterface.addColumn('products', 'formula', {
          type: Sequelize.TEXT
        })
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return [
        queryInterface.removeColumn('products', 'benefit'),
        queryInterface.removeColumn('products', 'research'),
        queryInterface.removeColumn('products', 'formula')
      ];
    });
  }
};
