'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('role_user', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            validate: {
              notEmpty: true
            }
          },
          role_id: {
            type: Sequelize.INTEGER,
            validate: {
              notEmpty: true
            }
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]).then(function() {
        return queryInterface.sequelize.transaction(function handleTransaction(t) {
          return Promise.all([
            queryInterface.addConstraint('role_user', ['user_id'], {
              type: 'FOREIGN KEY',
              references: {
                table: 'users',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            }),
            queryInterface.addConstraint('role_user', ['role_id'], {
              type: 'FOREIGN KEY',
              references: {
                table: 'roles',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            })
          ]);
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('role_user')]);
    });
  }
};
