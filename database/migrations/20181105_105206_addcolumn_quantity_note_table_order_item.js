'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.addColumn('order_items', 'quantity', {
          type: Sequelize.INTEGER
        }),
        queryInterface.addColumn('order_items', 'note', {
          type: Sequelize.Sequelize.TEXT('long')
        })
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.removeColumn('quantity'), queryInterface.removeColumn('note')]);
    });
  }
};
