'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('plans', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          interval: {
            type: Sequelize.STRING,
            allowNull: true
          },
          nickname: {
            type: Sequelize.STRING,
            allowNull: false
          },
          currency: {
            type: Sequelize.STRING,
            allowNull: false
          },
          amount: {
            type: Sequelize.FLOAT,
            allowNull: false
          },
          plan_id: {
            type: Sequelize.STRING,
            allowNull: false
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('plans')]);
    });
  }
};
