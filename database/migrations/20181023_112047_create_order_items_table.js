'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('order_items', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          order_id: {
            type: Sequelize.INTEGER,
            allowNull: false
          },
          product_id: {
            type: Sequelize.INTEGER,
            allowNull: false
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]).then(function() {
        return [
          queryInterface.addConstraint('order_items', ['product_id'], {
            type: 'FOREIGN KEY',
            references: {
              table: 'products',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          }),
          queryInterface.addConstraint('order_items', ['order_id'], {
            type: 'FOREIGN KEY',
            references: {
              table: 'orders',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
        ];
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('order_items')]);
    });
  }
};
