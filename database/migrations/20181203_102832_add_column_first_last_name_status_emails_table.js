'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.addColumn('emails', 'first_name', {
          type: Sequelize.TEXT
        }),
        queryInterface.addColumn('emails', 'last_name', {
          type: Sequelize.TEXT
        }),
        queryInterface.addColumn('emails', 'status', {
          type: Sequelize.INTEGER
        })
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return [
        queryInterface.removeColumn('emails', 'first_name'),
        queryInterface.removeColumn('emails', 'last_name'),
        queryInterface.removeColumn('emails', 'status')
      ];
    });
  }
};
