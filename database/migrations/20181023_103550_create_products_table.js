'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('products', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          name: {
            type: Sequelize.STRING
          },
          sku: {
            type: Sequelize.STRING
          },
          description: {
            type: Sequelize.TEXT('long')
          },
          short_description: {
            type: Sequelize.STRING
          },
          price: {
            type: Sequelize.FLOAT
          },
          status: {
            type: Sequelize.INTEGER
          },
          quantity: {
            type: Sequelize.INTEGER
          },
          color: {
            type: Sequelize.STRING
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('products')]);
    });
  }
};
