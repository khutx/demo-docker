'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('addresses', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
          },
          company: {
              type: Sequelize.STRING
          },
          address_line_1: {
            allowNull: false,
            type: Sequelize.STRING
          },
          address_line_2: {
            allowNull: false,
            type: Sequelize.STRING
          },
          city: {
            allowNull: false,
            type: Sequelize.STRING
          },
          state: {
            allowNull: false,
            type: Sequelize.STRING
          },
          country: {
            allowNull: false,
            type: Sequelize.STRING
          },
          zipcode: {
            allowNull: false,
            type: Sequelize.STRING
          },
          phone: {
            allowNull: false,
            type: Sequelize.STRING
          },
          is_default: {
            type: Sequelize.BOOLEAN
          },
          is_default_billing: {
            type: Sequelize.BOOLEAN
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]).then(function() {
        return queryInterface.addConstraint('addresses', ['user_id'], {
          type: 'FOREIGN KEY',
          references: {
            table: 'users',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });;
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('addresses')]);
    });
  }
};
