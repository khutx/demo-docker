'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('category_product', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          product_id: {
            type: Sequelize.INTEGER
          },
          category_id: {
            type: Sequelize.INTEGER
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]).then(function() {
        return [
          queryInterface.addConstraint('category_product', ['product_id'], {
            type: 'FOREIGN KEY',
            references: {
              table: 'products',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          }),
          queryInterface.addConstraint('category_product', ['category_id'], {
            type: 'FOREIGN KEY',
            references: {
              table: 'categories',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
        ];
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('category_product')]);
    });
  }
};
