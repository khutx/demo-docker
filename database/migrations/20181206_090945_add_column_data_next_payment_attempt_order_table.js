'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.addColumn('orders', 'next_payment_attempt', {
          type: Sequelize.TEXT
        })
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return [queryInterface.removeColumn('orders', 'next_payment_attempt')];
    });
  }
};
