'use strict';
var DB_CONST = require('../../config/const');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('customers', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER
          },
          first_name: {
            type: Sequelize.STRING
          },
          last_name: {
            type: Sequelize.STRING
          },
          birth: {
            type: Sequelize.DATE
          },
          group: {
            type: Sequelize.STRING
          },
          gender: {
            type: Sequelize.ENUM,
            values: DB_CONST.GENDER
          },
          phone: {
            type: Sequelize.STRING
          },
          phone_contact: {
            type: Sequelize.STRING
          },
          address: {
            type: Sequelize.STRING
          },
          shipping_address: {
            type: Sequelize.STRING
          },
          payment_id: {
            type: Sequelize.STRING
          },
          card_last_four: {
            type: Sequelize.STRING
          },
          card_brand: {
            type: Sequelize.STRING
          },
          card_expire: {
            type: Sequelize.STRING
          },
          referal_code: {
            type: Sequelize.STRING
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]).then(function() {
        return queryInterface.addConstraint('customers', ['user_id'], {
          type: 'FOREIGN KEY',
          references: {
            table: 'users',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        });
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('customers')]);
    });
  }
};
