'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([
        queryInterface.createTable('recommendations', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            allowNull: false
          },
          product_id: {
            type: Sequelize.INTEGER,
            allowNull: false
          },
          quantity: {
            type: Sequelize.INTEGER,
            allowNull: false
          },
          note: {
            type: Sequelize.STRING,
            allowNull: true
          },
          status: {
            type: Sequelize.INTEGER,
            allowNull: true
          },
          created_at: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            type: Sequelize.DATE
          }
        })
      ]).then(function() {
        return [
          queryInterface.addConstraint('recommendations', ['user_id'], {
            type: 'FOREIGN KEY',
            references: {
              table: 'users',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          }),
          queryInterface.addConstraint('recommendations', ['product_id'], {
            type: 'FOREIGN KEY',
            references: {
              table: 'products',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
        ];
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(function handleTransaction(t) {
      return Promise.all([queryInterface.dropTable('recommendations')]);
    });
  }
};
