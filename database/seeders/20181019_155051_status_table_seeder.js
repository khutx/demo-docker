'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        name: 'Pending',
        slug: 'pending'
      },
      {
        name: 'Active',
        slug: 'active'
      }
    ];

    return queryInterface.bulkInsert('status', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('status', null, {});
  }
};
