'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        interval: 'month',
        nickname: 'nick name 1',
        currency : 'usd',
        amount : 6000,
        plan_id : 'plan_Dyu2uUtiQbHO9f'
      },
      {
        interval: 'month',
        nickname: 'nick name 2',
        currency : 'usd',
        amount : 6000,
        plan_id : 'plan_DxQyADWK5mBrf1'
      }
    ];

    return queryInterface.bulkInsert('plans', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('plans', null, {});
  }
};
