'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        user_id: 1,
        product_id: 1,
        quantity: 2,
        note: 'no note',
        status: 0
      },
      {
        user_id: 2,
        product_id: 2,
        quantity: 3,
        note: 'no note',
        status: 0
      }
    ];

    return queryInterface.bulkInsert('recommendations', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('recommendations', null, {});
  }
};
