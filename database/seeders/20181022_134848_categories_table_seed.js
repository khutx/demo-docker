'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        name: 'Brain',
        slug: 'brain',
        parent_id : 0,
        image: '/assets/images/icon_brain.svg'
      },
      {
        name: 'Bones',
        slug: 'bones',
        parent_id : 0,
        image: '/assets/images/icon_bones.svg'
      },
      {
        name: 'Joints',
        slug: 'joints',
        parent_id : 0,
        image: '/assets/images/icon_joints.svg'
      },
      {
        name: 'Energy',
        slug: 'energy',
        parent_id : 0,
        image: '/assets/images/icon_energy.svg'
      },
      {
        name: 'Sleep',
        slug: 'sleep',
        parent_id : 0,
        image: '/assets/images/icon_sleep.svg'
      },
      {
        name: 'Heart',
        slug: 'heart',
        parent_id : 0,
        image: '/assets/images/icon_heart.svg'
      },
      {
        name: 'Stress',
        slug: 'stress',
        parent_id : 0,
        image: '/assets/images/icon_stress.svg'
      },
      {
        name: 'Immunity',
        slug: 'immunity',
        parent_id : 0,
        image: '/assets/images/icon_immunity.svg'
      },
      {
        name: 'Skin',
        slug: 'skin',
        parent_id : 0,
        image: '/assets/images/icon_skin.svg'
      },
      {
        name: 'Hair',
        slug: 'hair',
        parent_id : 0,
        image: '/assets/images/icon_hair.svg'
      },
      {
        name: 'Digestion',
        slug: 'digestion',
        parent_id : 0,
        image: '/assets/images/icon_digestion.svg'
      },
    ];

    return queryInterface.bulkInsert('categories', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('categories', null, {});
  }
};
