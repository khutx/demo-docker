'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
    let items = [
        {
            product_id : 1,
            category_id :2
        },
        {
            product_id : 1,
            category_id :3
        },
        {
            product_id : 2,
            category_id :1
        },
        {
            product_id : 2,
            category_id :4
        },
        {
            product_id : 3,
            category_id :5
        },
        {
            product_id : 3,
            category_id :2
        },
        {
            product_id : 3,
            category_id :3
        },
        {
            product_id : 3,
            category_id :6
        },
        {
            product_id : 3,
            category_id :7
        },
        {
            product_id : 4,
            category_id :6
        },
        {
            product_id : 4,
            category_id :1
        },
        {
            product_id : 5,
            category_id :4
        },
        {
            product_id : 5,
            category_id :8
        },
        {
            product_id : 5,
            category_id :9
        },
        {
            product_id : 5,
            category_id :10
        },
        {
            product_id : 6,
            category_id :4
        },
        {
            product_id : 6,
            category_id :10
        },
        {
            product_id : 6,
            category_id :11
        },
        {
            product_id : 7,
            category_id :11
        },
        {
            product_id : 8,
            category_id :7
        },
        {
            product_id : 8,
            category_id :9
        },
        {
            product_id : 8,
            category_id :10
        },
        {
            product_id : 8,
            category_id :8
        },
        {
            product_id : 9,
            category_id :2
        }
    ];

    return queryInterface.bulkInsert('category_product', items.map(item => Object.assign(item, {created_at: new Date(), updated_at: new Date()})));
    },

    down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('category_product', null, {});
    }
};