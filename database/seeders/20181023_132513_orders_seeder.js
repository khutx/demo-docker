'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        user_id: 1,
        payment_method: 'cash_on_delivery',
        amount: 3,
        status: 1,
        note: 'no node',
        shipping_method_id: 1
      },
      {
        user_id: 2,
        payment_method: 'cash_on_delivery',
        amount: 4,
        status: 1,
        note: 'no node',
        shipping_method_id: 1
      }
    ];

    return queryInterface.bulkInsert('orders', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    // return queryInterface.bulkDelete('table_name', null, {});
  }
};
