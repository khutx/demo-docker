'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('permission_groups', [
      {
        name: 'Product Management',
        slug: 'product_management',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Account Management',
        slug: 'account_management',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Order Management',
        slug: 'order_management',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Other',
        slug: 'other',
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('slugs', null, {});
  }
};
