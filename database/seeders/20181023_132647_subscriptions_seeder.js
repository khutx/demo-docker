'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        user_id: 1,
        plan_id: 1,
        start_time: new Date(),
        subscription_id: 'sub_DydpEXzVLUEZBV'
      },
      {
        user_id: 2,
        plan_id: 2,
        start_time: new Date(),
        subscription_id: 'sub_DxRrS9QgmGq4oX'
      }
    ];

    return queryInterface.bulkInsert('subscriptions', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('subscriptions', null, {});
  }
};
