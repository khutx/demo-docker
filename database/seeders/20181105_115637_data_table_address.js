'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
    let items = [
        {
            user_id : 1,
            company: 'vmms',
            address_line_1 : 'address 1',
            address_line_2 : 'address 2',
            city: 'Ha Noi',
            state: 'Ha Noi',
            country: 'Viet Nam',
            zipcode: 'ABC123',
            phone: '0379170680',
            is_default: false,
            is_default_billing: true
        },
        {
            user_id : 1,
            company: 'vmms',
            address_line_1 : 'address 1',
            address_line_2 : 'address 2',
            city: 'Hai Phong',
            state: 'Hung Yen',
            country: 'Viet Nam',
            zipcode: '123ABC',
            phone: '0379170680',
            is_default: false,
            is_default_billing: true
        }
    ];

    return queryInterface.bulkInsert('addresses', items.map(item => Object.assign(item, {created_at: new Date(), updated_at: new Date()})));
    },

    down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('addresses', null, {});
    }
};