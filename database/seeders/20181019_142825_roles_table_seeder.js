'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('roles', [
      {
        name: 'Super Admin',
        slug: 'superadmin',
        permissions: JSON.stringify([]),
        level: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Admin',
        slug: 'admin',
        permissions: JSON.stringify(['admin.view', 'admin.create', 'admin.update', 'admin.delete']),
        level: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'user',
        slug: 'user',
        level: 1,
        permissions: JSON.stringify([
          'order.view',
          'order.create',
          'order.update',
          'order.delete',
          'product.view',
          'product.create',
          'product.update',
          'product.delete',
          'user.update',
          'user.create',
          'user.view',
          'user.delete',
          'role.create',
          'role.update',
          'role.view',
          'role.delete'
        ]),
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('roles', null, {});
  }
};
