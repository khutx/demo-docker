'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('customers', [
      {
        user_id: 1,
        group: 'VIP',
        first_name: 'Marcel',
        last_name: 'Liu',
        birth: '1992-7-17',
        gender: 'male',
        phone: 123456789,
        phone_contact: '0977728137',
        address: 'nam hong-donganh-hanoi',
        shipping_address: 'namhong-donganh-hanoi',
        payment_id: 'cus_DxQyKyeqYEyPFl',
        card_last_four: 'XXX',
        card_brand: 'YYY',
        card_expire: '4',
        referal_code: '123',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 2,
        group: 'B2B',
        first_name: 'Hans',
        last_name: 'Tjipto',
        birth: '1992-7-17',
        gender: 'male',
        phone: 123456789,
        phone_contact: '0977728137',
        address: 'nam hong-donganh-hanoi',
        shipping_address: 'namhong-donganh-hanoi',
        payment_id: 'cus_DxUWKNWKzXC469',
        card_last_four: 'XXX',
        card_brand: 'YYY',
        card_expire: '4',
        referal_code: '123',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 3,
        group: 'DIY',
        first_name: 'Dave',
        last_name: 'Koz',
        birth: '1992-7-17',
        gender: 'male',
        phone: 123456789,
        phone_contact: '0977728137',
        address: 'nam hong-donganh-hanoi',
        shipping_address: 'namhong-donganh-hanoi',
        payment_id: 'cus_DxtmSIMFyHDp0X',
        card_last_four: 'XXX',
        card_brand: 'YYY',
        card_expire: '4',
        referal_code: '123',
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('customers', null, {});
  }
};
