'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
    let items = [
        {
            imageable_type: 'images demo 1',
            imageable_id: '1 ',
            url: 'htpp://localhost',
            type: 'png'
          },
          {
            imageable_type: 'images demo 2',
            imageable_id: '1 ',
            url: 'htpp://localhost',
            type: 'jpg'
          }
    ];

    return queryInterface.bulkInsert('images', items.map(item => Object.assign(item, {created_at: new Date(), updated_at: new Date()})));
    },

    down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('images', null, {});
    }
};