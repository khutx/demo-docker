'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        name: 'Calcium Plus',
        sku: 'Calcium Plus',
        description: 'Calcium is essential for your bones and joints. Our formula combines Calcium and Vitamin D for better absorption',
        short_description: '',
        price: 17,
        status: 1,
        quantity: 1,
        color: '#1abc9c',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
                <tr>
                  <td>Bones and Joints</td>
                  <td>Calcium is essential for bone and joint health and getting adequate levels throughout life may reduce the risk of osteoporosis</td>
                </tr>
                <tr>
                    <td>Diet & Lifestyle</td>
                    <td>
                    A diet poor in dairy may need to be supplemented with Calcium<br>
                    Smoking can deplete Calcium, so if you are a smoker you may need Calcium supplements<br>
                    Important when not getting enough sunshine: can result in Calcium deficiency, because Vitamin D (produced by the body when exposed to the sunshine) helps the body with Calcium absorption
                    </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'Vitamin B12',
        sku: 'Vitamin B12',
        description: "Vitamin B12 is a nutrient that helps keep the body's nerve and blood cells healthy and helps make DNA, the genetic material in all cells. ",
        short_description: '',
        price: 12,
        status: 1,
        quantity: 1,
        color: '#27ae60',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
            <tbody>
                <tr>
                  <td>Brain</td>
                  <td>Vitamin B12 is essential for brain functions</td>
                </tr>
                <tr>
                  <td>Diet & Lifestyle</td>
                  <td>
                    Important when not eating enough meat<br>
                    Important when on a Vegetarian / Vegan diet
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'Magnesium',
        sku: 'Magnesium',
        description: 'Magnesium helps to support a number of areas in body including muscle health, heart health and nervous system function',
        short_description: '',
        price: 12,
        status: 1,
        quantity: 2,
        color: '#8e44ad',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
                <tr>
                  <td>Bones</td>
                  <td>Magnesium plays a role in the regulation of calcium, making it essential for bone health</td>
                </tr>
                <tr>
                  <td>Heart</td>
                  <td>Magnesium is necessary to maintain the health of muscles, including the heart, and for the transmission of electrical signals in the body.</td>
                </tr>
                <tr>
                  <td>Sleep</td>
                  <td>Magnesium acts upon the nervous system and contributes to deep, restful sleep. Several studies have confirmed this effect in older adults.</td>
                </tr>
                <tr>
                <td>Diet & Lifestyle</td>
                <td>
                  Stress and anxiety: Reductions in magnesium levels, or changes in the way that it is processed, have been linked to increased levels of anxiety.<br>
                  Helps with muscle recovery after exercise<br>
                  Smoking can deplete Magnesium<br>
                  Alcohol can deplete Magnesium
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'Fish Oil',
        sku: 'Fish Oil',
        description: 'A high quality source of omega-3 helps maintain normal heart, brain and eye health and promote optimal wellbeing',
        short_description: '',
        price: 11,
        status: 1,
        quantity: 1,
        color: '#e67e22',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
              <tr>
                <td>Heart</td>
                <td>Fish oil may protect the heart and helps retain healthy triglyceride levels</td>
              </tr>
              <tr>
                <td>Brain</td>
                <td>The omega-3 fatty acids are critical for normal brain function and development throughout all stages of life</td>
              </tr>
              <tr>
                <td>Diet & Lifestyle</td>
                <td>
                  Important when not eating enough fish
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'Vitamin C',
        sku: 'Vitamin C',
        description: "Vitamin C, also known as ascorbic acid, is necessary for the growth, development and repair of all body tissues. It's involved in many body functions.",
        short_description: '',
        price: 11,
        status: 1,
        quantity: 1,
        color: '#f39c12',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
                <tr>
                  <td>Energy</td>
                  <td>your body needs vitamin C to make L-carnitine, which helps you burn fat for energy. Researchers at the National Institutes of Health reported that fatigue was one of the first signs of vitamin C depletion</td>
                </tr>
                <tr>
                  <td>Immunity</td>
                  <td>Vitamin C is an essential nutrient for healthy immune system response.. A vitamin C deficiency results in a reduced resistance against certain pathogens whilst a higher supply enhances several immune system parameters</td>
                </tr>
                <tr>
                  <td>Skin and Hair</td>
                  <td>You need vitamin C to make collagen, the predominant protein in the body. Collagen forms part of all our tissues, including skin, organs, cartilage, and bone</td>
                </tr>
                <tr>
                  <td>Diet & Lifestyle</td>
                  <td>
                    A diet poor in dairy may need to be supplemented with Vitamin C<br>
                    Smoking can deplete Vitamin C
                  </td>
               </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'B complex',
        sku: 'B complex',
        description: 'Our B complex formula supports energy production and assists in times of high physical activity',
        short_description: '',
        price: 12,
        status: 1,
        quantity: 1,
        color: '#c0392b',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
                <tr>
                  <td>Energy</td>
                  <td>some studies suggest that B-complex vitamins can lift your spirits and improve your cognitive performance.</td>
                </tr>
                <tr>
                  <td>Hair</td>
                  <td>B-vitamins help create red blood cells, which carry oxygen and nutrients to the scalp and hair follicles. These processes are important for hair growth</td>
                </tr>
                <tr>
                  <td>Digestion</td>
                  <td>B vitamins help breakdown protein, carbohydrate and fats from your diet. They are water-soluble, meaning you can't store them away in your fat cells to use later; they need to be a regular part of your diet.</td>
                </tr>
                <tr>
                  <td>Diet & Lifestyle</td>
                  <td>
                    Important when not eating enough fruit and vegetables<br>
                    Important when on a Gluten free diet<br>
                    Alcohol can deplete B complex<br>
                    Smoking can deplete B complex
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'Probiotics',
        sku: 'Probiotics',
        description: 'Helps restoring and replenishing the good, natural bacteria in the digestive system for enhanced digestion',
        short_description: '',
        price: 17,
        status: 1,
        quantity: 1,
        color: '#2c3e50',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
                <tr>
                  <td>Digestion</td>
                  <td>the first major benefit of probiotics is as a promoter of good digestive health</td>
                </tr>
                <tr>
                  <td>Immunity</td>
                  <td>Probiotics may have potential for preventing wide scope of immunity related diseases due to anti-inflammatory effect</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'Zinc',
        sku: 'Zinc',
        description: 'Important in the proper functioning of the immune system, and the maintenance healthy hair and skin',
        short_description: '',
        price: 11,
        status: 1,
        quantity: 1,
        color: '#f1c40f',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
                <tr>
                  <td>Immunity</td>
                  <td>Zinc plays an essential role in controlling and regulating immune responses. Zinc deficiency can severely impair immune system function.</td>
                </tr>
                <tr>
                  <td>Skin & Hair</td>
                  <td>Zinc is essential to help maintain healthy nails, hair and skin</td>
                </tr>
                <tr>
                  <td>Diet & Lifestyle</td>
                  <td>
                    Stress depletes Zinc<br>
                    Important when eating little or no fish
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      },
      {
        name: 'Vitamin D',
        sku: 'Vitamin D',
        description: 'Vitamin D3 promotes the absorption of calcium and can help to support healthy muscles',
        short_description: '',
        price: 12,
        status: 1,
        quantity: 1,
        color: '#9b59b6',
        benefit: `
        <div class="row">
          <div class="col-12">
            <table class="table table-no-border">
              <tbody>
                <tr>
                  <td>Bones</td>
                  <td>without Vitamin D, our bodies cannot effectively absorb calcium, which is essential to good bone health.</td>
                </tr>
                <tr>
                  <td>Heart</td>
                  <td>previous research has shown that vitamin D deficiency may increase the risk of heart disease, heart attack and stroke</td>
                </tr>
                <tr>
                  <td>Diet & Lifestyle</td>
                  <td>
                    Important when not getting enough sunshine
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        `,
        research: '',
        formula: ''
      }
    ];

    return queryInterface.bulkInsert('products', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('products', null, {});
  }
};
