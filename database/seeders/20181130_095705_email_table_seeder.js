'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        email: 'nutrio@vicoders.com'
      }
    ];

    return queryInterface.bulkInsert('emails', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('emails', null, {});
  }
};
