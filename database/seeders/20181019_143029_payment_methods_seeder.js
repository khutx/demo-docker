'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        name: 'cash',
        availability: 'collect',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'transfer',
        availability: 'send',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'paypal',
        availability: 'send',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'international transfer',
        availability: 'send',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'stripe',
        availability: 'send',
        created_at: new Date(),
        updated_at: new Date()
      }
    ];

    return queryInterface.bulkInsert('payment_methods', items);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('payment_methods', null, {});
  }
};
