'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        title: 'What is Nutrio and how do I get started ?',
        content:
          'Nutrio delivers monthly personalised boxes of vitamins. Each month, we send 30 sachets with daily vitamins chosen just for you, based on a short quiz. Take the quiz now to find out your own personalised recommendation.'
      },
      {
        title: 'Do I have to subscribe to get my personalised vitamins ?',
        content: "Nutrio offers flexible subscriptions only. It means that you can adjust or cancel at any time! However, if you want to try us out for a month, you can email us right after you've placed your order, and we will pause your subscription."
      },
      {
        title: 'How can I cancel/pause my subscription ?',
        content: "Yes, you can cancel and pause at any time. Just email us and we will sort it out for you !"
      },
      {
        title: 'What is inside my monthly vitamin box ?',
        content: "You will receive 30 personalised sachets in a nice looking dispenser. Each sachet contains your daily dose of vitamins. It could be 2, 3, 4 capsules and/or tablets depending on what you need! You’ll also receive a booklet explaining what each tablet is and what they contain, so you can find all the info your need."
      },
      {
        title: 'How does pricing work ?',
        content: "We show you the daily dose and monthly price of each vitamin or supplement on the product page and on your recommendation page. For example, Magnesium is $12 per month, and you'll take one tablet per day. We deliver 30 sachets monthly, so the price of Magnesium is $0.40 per day! Feel free to email us at hello@nutriohealth.co with any pricing questions."
      },
      {
        title: 'What is the status of my order ?',
        content: "You will be notified a few days before your next order finalises, and you'll have two days to make any changes you want before your card is charged and the order ships. Once your order ships you'll get an email with tracking information."
      },
      {
        title: 'Where do you deliver ?',
        content: "We deliver to most areas in Australia. Get in touch if you are unsure if we can deliver to your house!"
      },
      {
        title: 'How long will it take for my order to arrive ?',
        content: "It usually takes 5-8 days for orders to be delivered. As soon as your order is shipped, you will receive an email with tracking information so you can get an estimated delivery date for your order."
      },
      {
        title: 'When will I receive my box each month ?',
        content: "You will receive your box every 30 days, from the first day you placed your order."
      },
      {
        title: 'How much does shipping cost ?',
        content: "Shipping is free for orders $30 and up, and $12.95 for orders under $30."
      },
      {
        title: 'How does personalisation work ?',
        content: "We work with a team of health experts"
      },
      {
        title: 'Can I change my answer to the quiz ?',
        content: "We are working on an easy way for you to get back to the quiz and only edit your answers. For now, you need to re-take the quiz if you want to change what you’ve told us! But you can always adjust your pack later and add/remove products to your recommendations. Get in touch if you have any questions: hello@nutriohealth.co"
      },
      {
        title: 'Can I change the content of my box ?',
        content: "You can edit your box by adding and/or removing products as you want. You can find all the information you need on our product page, so you can decide what product you’d like to add to your box."
      },
      {
        title: 'Can I change the email address on my account ?',
        content: "You can, but not on our website. Email us at hello@nutriohealth.co and we'll do it for you !"
      },
      {
        title: 'I forgot my password',
        content: "No problem! Just head to the login area on our website and click on “I forgot my password”. We will help you retrieve it!"
      },
      {
        title: 'Can I update my delivery address ?',
        content: "Yes you can! Log in to your account and change your address in your settings for future orders. If an order has already processed, this change will only affect your next order."
      },
      {
        title: 'Can I change the frequency of my orders ?',
        content: "Our deliveries are made every 30 days. If you wish to pause your subscription, you can go to your account and pause it until you are ready for your next order !"
      },
      {
        title: 'Can I cancel an order ?',
        content: "Yes. You can cancel your subscription at any time. If your order has already been processed, the cancellation will affect your next order."
      },
      {
        title: 'How can I cancel my subscription ?',
        content: "We would be very sad to see you go! If you decide to cancel, just drop us a quick email at hello@nutriohealth.co from the same email as your account. Feel free to include feedback as to why you are cancelling so we can help you out!"
      },
      {
        title: 'I received a promo code. How do I redeem it ?',
        content: "Go to our website https://www.nutriohealth.co, take the quiz or browse our product list and select what you’d like to get in your box. When you get to the delivery / billing section, you’ll get the option to enter your promo code on the right. The code will be applied to your total."
      },

    ];

    return queryInterface.bulkInsert('faqs', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('faqs', null, {});
  }
};
