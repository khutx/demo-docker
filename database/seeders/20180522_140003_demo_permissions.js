'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('permissions', [
      {
        name: 'Admin View',
        slug: 'admin.view',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Admin Create',
        slug: 'admin.create',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Admin Update',
        slug: 'admin.update',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Admin Delete',
        slug: 'admin.delete',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'User View',
        slug: 'user.view',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'User Create',
        slug: 'user.create',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'User Update',
        slug: 'user.update',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'User Delete',
        slug: 'user.delete',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Role View',
        slug: 'role.view',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Role Create',
        slug: 'role.create',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Role Update',
        slug: 'role.update',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },

      {
        name: 'Role Delete',
        slug: 'role.delete',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Product View',
        slug: 'product.view',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Product Create',
        slug: 'product.create',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Product Update',
        slug: 'product.update',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Product Delete',
        slug: 'product.delete',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Customer View',
        slug: 'customer.view',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Customer Create',
        slug: 'customer.create',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Customer Update',
        slug: 'customer.update',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Customer Delete',
        slug: 'customer.delete',
        group_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Category View',
        slug: 'category.view',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Category Update',
        slug: 'category.update',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Category Create',
        slug: 'category.create',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Category Delete',
        slug: 'category.delete',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Dataset View',
        slug: 'dataset.view',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Dataset Create',
        slug: 'dataset.create',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Dataset Update',
        slug: 'dataset.update',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Dataset Delete',
        slug: 'dataset.delete',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Order View',
        slug: 'order.view',
        group_id: 3,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Order Create',
        slug: 'order.create',
        group_id: 3,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Order Update',
        slug: 'order.update',
        group_id: 3,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Order Delete',
        slug: 'order.delete',
        group_id: 3,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Status Create',
        slug: 'status.create',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Status Update',
        slug: 'status.update',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: 'Status Delete',
        slug: 'status.delete',
        group_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('permissions', null, {});
  }
};
