'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let items = [
      {
        order_id: 1,
        product_id: 1,
        quantity: 3,
        note: 'note 1'
      },
      {
        order_id: 2,
        product_id: 2,
        quantity: 10,
        note: 'note 100'
      }
    ];

    return queryInterface.bulkInsert('order_items', items.map(item => Object.assign(item, { created_at: new Date(), updated_at: new Date() })));
  },

  down: (queryInterface, Sequelize) => {
    // return queryInterface.bulkDelete('table_name', null, {});
  }
};
