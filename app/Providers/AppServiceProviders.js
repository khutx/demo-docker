import User from '../Models/User';
import Role from '../Models/Role';
import { Authenticate } from '@nsilly/auth';
import { RequestParser, ServiceProvider } from '@nsilly/support';
import { App } from '@nsilly/container';
import { Stripe } from '../Services/Payment/Gateways/Stripe/Stripe';

export default class AppServiceProvider extends ServiceProvider {
  register() {
    App.singleton('Auth', Authenticate);
    App.singleton('Request', RequestParser);
    App.singleton('PaymentGateway', Stripe);
  }
  boot() {
    App.make('Auth').setModel({ user: User, role: Role });
  }
}
