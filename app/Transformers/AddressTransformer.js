import Transformer from './Transformer';

export default class AddressTransformer extends Transformer {
  transform(model) {
    if (model) {
      return {
        id: model.id,
        user_id: model.user_id,
        company: model.company,
        address_line_1: model.address_line_1,
        address_line_2: model.address_line_2,
        city: model.city,
        state: model.state,
        zipcode: model.zipcode,
        phone: model.phone,
        country: model.country,
        is_default: model.is_default,
        is_default_billing: model.is_default_billing
      };
    } else {
      return null;
    }
  }
}
