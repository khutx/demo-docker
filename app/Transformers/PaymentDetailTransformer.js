import Transformer from './Transformer';
import PaymentMethodTransformer from './PaymentMethodTransformer';

export default class PaymentDetailTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      data: model.data,
      payment_method_id: model.payment_method_id,
      created_at: model.created_at,
      updated_at: model.updated_at
    };
  }
  includePaymentMethod(model) {
    return this.item(model.payment_method, new PaymentMethodTransformer());
  }
}
