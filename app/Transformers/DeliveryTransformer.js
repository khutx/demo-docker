import Transformer from './Transformer';
import UserTransformer from './UserTransformer';
import DeliveryAddressTransformer from './DeliveryAddressTransformer';
import ProductTransformer from './ProductTransformer';
import OrderTransformer from './OrderTransformer';

export default class DeliveryTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      object_id: model.object_id,
      object_type: model.object_type,
      delivery_type: model.delivery_type,
      status: model.status,
      date: model.date,
      note: model.note,
      data: model.data,
      delivered_by: model.delivered_by,
      user_id: model.user_id,
      tries: model.tries,
      display_name: model.display_name,
      expected_date: model.expected_date,
      received_date: model.received_date
    };
  }
  includeUser(model) {
    return this.item(model.user, new UserTransformer(['customer']));
  }
  includeDeliveryAddress(model) {
    return this.item(model.delivery_address, new DeliveryAddressTransformer());
  }
  includeProduct(model) {
    return this.item(model.product, new ProductTransformer());
  }
  includeOrder(model) {
    return this.item(model.order, new OrderTransformer());
  }
}
