import Transformer from './Transformer';

export default class FileTransformer extends Transformer {
  transform(model) {
    return {
      originalname: model.originalname,
      mimetype: model.mimetype,
      encoding: model.encoding,
      filename: model.filename,
      size: model.size,
      full_path: model.location
    };
  }
}
