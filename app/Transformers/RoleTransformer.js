import Transformer from './Transformer';

class PermissionTransformer extends Transformer {
  transform(model) {
    return {
      slug: model
    };
  }
}

export default class RoleTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      name: model.name,
      slug: model.slug,
      level: model.level
    };
  }

  includePermissions(model) {
    let permissions = model.permissions;
    if (typeof permissions === 'string') {
      permissions = JSON.parse(permissions);
    }
    return this.collection(permissions, new PermissionTransformer());
  }
}
