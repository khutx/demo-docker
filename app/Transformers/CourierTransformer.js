import Transformer from './Transformer';

export default class CourierTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      code: model.code,
      name: model.name,
      tracking_url: model.tracking_url
    };
  }
}
