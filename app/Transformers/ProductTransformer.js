import Transformer from './Transformer';
import CategoryTransformer from './CategoryTransformer';
import ImageTransformer from './ImageTransformer';
import OrderItemTransformer from './OrderItemTransformer';

export default class ProductTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      sku: model.sku,
      name: model.name,
      description: model.description,
      short_description: model.short_description,
      price: model.price,
      status: model.status,
      quantity: model.quantity,
      color: model.color,
      benefit: model.benefit,
      research: model.research,
      formula: model.formula,
      weight: model.weight
    };
  }

  includeCategories(model) {
    return this.collection(model.categories, new CategoryTransformer());
  }

  includeOrderItem(model) {
    return this.item(model.order_item, new OrderItemTransformer());
  }

  includeImages(model) {
    return this.collection(model.images, new ImageTransformer());
  }
}
