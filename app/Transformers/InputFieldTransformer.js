import Transformer from './Transformer';
export default class InputFieldTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      name: model.name,
      is_required: model.is_required,
      created_at: model.created_at,
      updated_at: model.updated_at
    };
  }
}
