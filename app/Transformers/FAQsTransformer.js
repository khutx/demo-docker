import Transformer from './Transformer';

export default class FAQsTransformer extends Transformer {
  transform(model) {
    return {
      title: model.title,
      content: model.content
    };
  }
}
