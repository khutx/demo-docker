import Transformer from './Transformer';

export default class ImageTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      imageable_type: model.imageable_type,
      imageable_id: model.imageable_id,
      url: model.url,
      type: model.type
    };
  }
}
