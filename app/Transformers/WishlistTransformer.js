import Transformer from './Transformer';
import UserTransformer from './UserTransformer';
import ProductTransformer from './ProductTransformer';
export default class WishlistTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      created_at: model.created_at,
      updated_at: model.updated_at
    };
  }

  includeUser(model) {
    return this.item(model.user, new UserTransformer());
  }

  includeProduct(model) {
    return this.item(model.product, new ProductTransformer());
  }
}
