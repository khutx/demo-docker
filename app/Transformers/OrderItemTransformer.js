import Transformer from './Transformer';
import ProductTransformer from './ProductTransformer';

export default class OrderItemTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      quantity : model.quantity,
      note: model.note
    };
  }

  includeProducts(model) {
    return this.item(model.product, new ProductTransformer());
  }
}
