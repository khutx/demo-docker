import Transformer from './Transformer';
import AddressTransformer from './AddressTransformer';
import UserTransformer from './UserTransformer';
import PaymentDetailTransformer from './PaymentDetailTransformer';

export default class CustomerTransformer extends Transformer {
  transform(model) {
    const pict = model.media ? model.media.find(s => s.main === true) : null;
    return {
      id: model.id,
      user_id: model.user_id,
      email: model.email,
      first_name: model.first_name,
      last_name: model.last_name,
      group: model.group,
      birth: model.birth,
      gender: model.gender,
      phone: model.phone,
      phone_contact: model.phone_contact,
      address: model.address,
      shipping_address: model.shipping_address,
      payment_id: model.payment_id,
      card_last_four: model.card_last_four,
      card_brand: model.card_brand,
      card_expire: model.card_expire,
      referal_code: model.referal_code,
      profile_picture: pict ? pict.relative_path : null,
      private_note: model.private_note
    };
  }

  includeAddresses(model) {
    return this.collection(model.addresses, new AddressTransformer());
  }

  includeUser(model) {
    return this.item(model.user, new UserTransformer());
  }

  includeDefaultPaymentDetail(model) {
    return this.item(model.default_payment_detail, new PaymentDetailTransformer(['payment_method']));
  }

  includePaymentDetails(model) {
    return this.collection(model.payment_details, new PaymentDetailTransformer(['payment_method']));
  }
}
