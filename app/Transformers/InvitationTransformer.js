import Transformer from './Transformer';

export default class InvitationTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      email: model.email
    };
  }
}
