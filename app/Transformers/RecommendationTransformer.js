import Transformer from './Transformer';
import ProductTransformer from './ProductTransformer';

export default class RecommendationTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      user_id: model.user_id,
      product_id: model.product_id,
      quantity: model.quantity,
      note: model.note,
      status: model.status
    };
  }
  includeProduct(model) {
    return this.item(model.product, new ProductTransformer(['images', 'categories']));
  }
}
