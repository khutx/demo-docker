import Transformer from './Transformer';

export default class PlanTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      interval: model.interval,
      nickname: model.nickname,
      currency: model.currency,
      amount: model.amount,
      plan_id : model.plan_id
    };
  }
}
