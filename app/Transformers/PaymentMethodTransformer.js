import Transformer from './Transformer';
import InputFieldTransformer from './InputFieldTransformer';

export default class PaymentMethodTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      name: model.name,
      availability: model.availability,
      required_value: model.input_fields ? model.input_fields.map(s => s.name) : [],
      created_at: model.created_at,
      updated_at: model.updated_at
    };
  }

  includeInputFields(model) {
    return this.collection(model.input_fields, new InputFieldTransformer());
  }
}
