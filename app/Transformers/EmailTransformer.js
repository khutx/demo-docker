import Transformer from './Transformer';

export default class EmailTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      email: model.email,
      first_name: model.first_name,
      last_name: model.last_name,
      status: model.status
    };
  }
}
