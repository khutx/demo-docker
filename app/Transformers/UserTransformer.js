import Transformer from './Transformer';
import CustomerTransformer from './CustomerTransformer';
import RoleTransformer from './RoleTransformer';
import OrderTransformer from './OrderTransformer';
import WishlistTransformer from './WishlistTransformer';
import RecommendationTransformer from './RecommendationTransformer';
import ImageTransformer from './ImageTransformer';
import ProductTransformer from './ProductTransformer';
import SubscriptionTransformer from './SubscriptionTransformer';
import AddressTransformer from './AddressTransformer';
export default class UserTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      email: model.email,
      status: model.status,
      last_login: model.last_login,
      created_at: model.created_at,
      updated_at: model.updated_at
    };
  }

  includeCustomer(model) {
    return this.item(model.customer, new CustomerTransformer(['addresses', 'default_payment_detail', 'payment_details']));
  }

  includeRoles(model) {
    return this.collection(model.roles, new RoleTransformer());
  }

  includeProducts(model) {
    return this.collection(model.products, new ProductTransformer());
  }

  includeRecommendations(model) {
    return this.collection(model.recommendations, new RecommendationTransformer(['product']));
  }

  includeSubscription(model) {
    return this.item(model.subscription, new SubscriptionTransformer(['plan']));
  }

  includeOrders(model) {
    return this.collection(model.orders, new OrderTransformer());
  }

  includeWishlists(model) {
    return this.collection(model.wishlist, new WishlistTransformer(['product']));
  }
  includeImages(model) {
    return this.collection(model.images, new ImageTransformer());
  }
  includeAddresses(model) {
    return this.collection(model.addresses, new AddressTransformer());
  }
}
