import Transformer from './Transformer';
import ProductTransformer from './ProductTransformer';
import UserTransformer from './UserTransformer';
import OrderItemTransformer from './OrderItemTransformer';

export default class OrderTransformer extends Transformer {
  transform(model) {
    if (model) {
      return {
        id: model.id,
        user_id: model.user_id,
        payment_method: model.payment_method,
        amount: model.amount,
        note: model.note,
        shipping_method_id: model.shipping_method_id,
        status: model.status,
        next_payment_attempt: model.next_payment_attempt,
        create_at: model.create_at
      };
    } else {
      return null;
    }
  }

  includeUser(model) {
    return this.item(model.user, new UserTransformer());
  }

  includeProducts(model) {
    return this.collection(model.products, new ProductTransformer(['order_item']));
  }

  includeOrderItems(model) {
    return this.collection(model.order_items, new OrderItemTransformer(['products']));
  }
}
