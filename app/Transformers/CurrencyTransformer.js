import Transformer from './Transformer';

export default class CurrencyTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      code: model.code,
      symbol: model.symbol,
      rate: model.rate,
      base: model.base,
      country: model.country
    };
  }
}
