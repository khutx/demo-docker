import Transformer from './Transformer';
import UserTransformer from './UserTransformer';
import PlanTransformer from './PlanTransformer';

export default class SubscriptionTransformer extends Transformer {
  transform(model) {
    if (model) {
      return {
        id: model.id,
        start_time: model.start_time,
        subscription_id: model.subscription_id,
        status: model.status
      };
    } else {
      return null;
    }
  }
  includeUser(model) {
    return this.item(model.user, new UserTransformer());
  }
  includePlan(model) {
    return this.item(model.plan, new PlanTransformer());
  }
}
