import Transformer from './Transformer';

export default class PermissionTransformer extends Transformer {
  transform(model) {
    return {
      id: model.id,
      slug: model.slug,
      name: model.name
    };
  }
}
