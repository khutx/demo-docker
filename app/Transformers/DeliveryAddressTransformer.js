import Transformer from './Transformer';

export default class DeliveryAddressTransformer extends Transformer {
  transform(model) {
    const { id, company, address_1, address_2, city, state, zipcode, phone_code, phone, country, first_name, last_name } = model;

    return { id, company, address_1, address_2, city, state, zipcode, phone_code, phone, country, first_name, last_name };
  }
}
