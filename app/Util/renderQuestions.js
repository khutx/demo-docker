import * as _ from 'lodash';
import Questions from './questions';

export const renderQuestions = function(questions, answers) {
  let recommendations = [];
  let customer = {};
  for (const question of questions) {
    const conten = _.find(Questions, ['title', question.title]);
    if (conten) {
      const answer = _.find(answers, { field: { id: question.id } });
      if (answer) {
        if (conten.title === "What's your<strong> first name</strong> (or nickname)?") {
          customer = _.assign(customer, conten.actions(answer));
        } else if (conten.title === 'Which email address should we use?') {
          customer = _.assign(customer, conten.actions(answer));
        } else {
          if (answer.type === 'boolean') {
            const data = conten.actions(answer.boolean);
            if (!_.isNil(data) && !_.isFunction(data)) {
              recommendations = [...recommendations, ...data];
            } else if (_.isFunction(data)) {
              recommendations.push(data);
            }
          }
          if (answer.type === 'number') {
            const data = conten.actions(answer.number);
            if (!_.isNil(data) && !_.isFunction(data)) {
              recommendations = [...recommendations, ...data];
            } else if (_.isFunction(data)) {
              recommendations.push(data);
            }
          }
          if (answer.type === 'choice') {
            const data = conten.actions(answer.choice.label);
            if (!_.isNil(data) && !_.isFunction(data)) {
              recommendations = [...recommendations, ...data];
            } else if (_.isFunction(data)) {
              recommendations.push(data);
            }
          }
          if (answer.type === 'choices') {
            const data = conten.actions(answer.choices.labels);
            if (!_.isNil(data) && !_.isFunction(data)) {
              recommendations = [...recommendations, ...data];
            } else if (_.isFunction(data)) {
              recommendations.push(data);
            }
          }
          if (answer.type === 'rating') {
          }
          if (answer.type === 'email') {
            const data = conten.actions(answer);
            customer.email = data;
          }
          if (answer.type === 'text') {
            const data = conten.actions(answer);
            customer.first_name = data;
          }
        }
      }
    }
  }
  for (const item of recommendations) {
    if (_.isFunction(item)) {
      const data = item(recommendations);
      recommendations = [...data];
    }
  }

  return { customer: customer, recommendations: recommendations };
};
