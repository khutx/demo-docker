import _ from 'lodash';

const Questions = [
  {
    title: "What's your<strong> first name</strong> (or nickname)?",
    actions: function(answers) {
      return { first_name: answers.text };
    }
  },
  {
    title: 'Which email address should we use?',
    actions: function(answers) {
      return { email: answers.email };
    }
  },
  {
    id: 'yKVTk8zd0Yvi',
    title: 'Does your mood fluctuate:',
    ref: '2295a57d-7e9d-4d68-aff1-eb309c45a847',
    select: {
      options: ['All the time'],
      result: [{ sku: 'Fish Oil', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'hhuMNHI3RFFM',
    title: 'Do you have trouble focusing?',
    ref: '68ae4436-20e5-4fe7-89d2-e9e190c41fe7',
    select: {
      options: [true],
      result: [{ sku: 'Fish Oil', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'q1n2B01y8Aza',
    title: 'Do you experience brain fog?',
    ref: '556abb4d-327d-4271-a9d4-629a06012531',
    select: {
      options: [true],
      result: [{ sku: 'Fish Oil', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'OkuHbMNa93I4',
    title: 'Are your concerned about your short term memory?',
    ref: '9d800ec2-d200-4e01-ab11-6cafb6488520',
    select: {
      options: [true],
      result: [{ sku: 'Fish Oil', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'h9NzLFRLhBMc',
    title: 'Any heart concerns?',
    ref: '96d83452-3a9f-4f75-b4bf-de88ae0aaea1',
    select: {
      options: ['High cholesterol', 'High blood pressure'],
      result: [{ sku: 'Fish Oil', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'B72JjPGpKH9y',
    title: 'Are you taking blood thinners?',
    ref: '7eca3b82-5548-47f3-8a54-52f6ac678620',
    select: {
      options: [true],
      result: [{ sku: 'Fish Oil', quantity: 1, note: 'Only recommend 1.5g Fish Oil max per day' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'a1bkwUnK7YHl',
    title: 'Do you have a family history of bone issues?',
    ref: '4abd7811-d1f8-4e54-8b83-762a4f660d5c',
    select: {
      options: [true],
      result: [{ sku: 'Calcium Plus', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'JKeYLMIdRj1U',
    title: 'Do you feel like you easily get sick?',
    ref: '2e705e84-1387-4c45-b973-f2879b31ebdb',
    select: {
      options: [true],
      result: [{ sku: 'Vitamin C', quantity: 1, note: '' }, { sku: 'Zinc', quantity: 1, note: '' }, { sku: 'B Complex', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'cPxBsz20SH3q',
    title: 'Do you often get cold sores?',
    ref: '7652327a-f0d5-491c-ad1d-611cb556d2bd',
    select: {
      options: [true],
      result: [{ sku: 'Vitamin C', quantity: 1, note: '' }, { sku: 'Zinc', quantity: 1, note: '' }, { sku: 'B Complex', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'NY9VHn917u2Q',
    title: 'Do you follow any of the below diets?',
    ref: '5c5a42b0-8e7a-43ca-936b-77ed951e6d32',
    select1: {
      options: ['Vegetarian', 'Vegan'],
      result: [
        function(array) {
          for (const item of array) {
            if (item.sku === 'Fish Oil') {
              item.sku = 'Omega 3';
            }
          }
          return array;
        },
        { sku: 'Omega 3', quantity: 1, note: 'NO Fish Oil, replace with Omega 3 - correct.' },
        { sku: 'Vitamin B-12', quantity: 1, note: '' }
      ]
    },
    select2: {
      options: ['Pescetarian'],
      result: [{ sku: 'Vitamin B-12', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      let result = [];
      for (const item of answers) {
        if (_.includes(this.select1.options, item)) {
          result = [...result, this.select1.result];
        }
        if (_.includes(this.select2.options, item)) {
          result = [...result, this.select2.result];
        }
      }
      return _.flatten(result);
    }
  },
  {
    id: 'LRGoiJ57AMHe',
    title: 'How often do you eat fish?',
    ref: '2c50eaa9-2fc5-4ec5-af51-9aa2237783f2',
    select: {
      options: ['Never'],
      result: [{ sku: 'Fish Oil', quantity: 1, note: 'Fish Oil or Omega 3 (vegan)' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'M7qugtCCsCvm',
    title: 'How often do you eat red meat?',
    ref: 'e826e40b-d79c-47b3-87e3-f1a43fafc6d5',
    select: {
      options: ['Never'],
      result: [{ sku: 'Vitamin B-12', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'pQS25q03XKDz',
    title: 'How many serves of fruit and vegetables per day do you eat?',
    ref: 'd28aa1d1-2891-4dfb-97be-2c699557d77b',
    select: {
      options: ['None', '1-2'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'pudGa9ogNAzr',
    title: 'How often do you eat dairy?',
    ref: '4caa9ce2-9b29-4fdc-93be-dbb41010525a',
    select: {
      options: ['Never'],
      result: [{ sku: 'Calcium Plus', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'Nab3s0dQDRbY',
    title: 'Do you have any of these allergies?',
    ref: '6bf6c669-dd3d-4bce-9467-91c6af8d5fb5',
    select: {
      options: ['Fish'],
      result: function(array) {
        for (const item of array) {
          if (item.sku === 'Fish Oil') {
            item.sku = 'Omega 3';
          }
        }
        return array;
      }
    },
    actions: function(answers) {
      let result = null;
      for (const item of answers) {
        if (_.includes(this.select.options, item)) {
          result = this.select.result;
        }
      }
      return result;
    }
  },
  {
    id: 'yJUxoduntvXH',
    title: 'How many days per week do you exercise on average?',
    ref: 'e7674bc2-2cb1-44cf-b729-13215728f31f',
    select: {
      options: ['3-5', 'Almost daily'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'StMTE9o34CLs',
    title: 'How would you rate the intensity of the exercise you engage in? (1 being mellow like low intensity yoga and 5 being high intensity exercise, like sprints)',
    ref: 'b04c9761-207e-487e-8fd5-dd22c6cf1212',
    select: {
      options: ['4', '5'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'q7wEDquVGMgo',
    title: 'How long does each session last on average?',
    ref: '07dc9a95-8950-4c86-a67c-99fd5dc8b79e',
    select: {
      options: ['More than 45min'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'gWqzNL2HS4Vs',
    title: 'Do you get at least 10 min of sunshine daily?',
    ref: 'a606a33d-6b5d-4017-ad5d-110713dc3d49',
    select: {
      options: [false],
      result: [{ sku: 'Vitamin D', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'e3up7URS4mwV',
    title: 'How many drinks would you consume on average, per week?',
    ref: 'fed2f968-91a2-43d1-bd1d-ce4fc0b0fb95',
    select1: {
      options: ['More than 6'],
      result: [{ sku: 'B complex', quantity: 1, note: '' }, { sku: 'Vitamin C', quantity: 1, note: '' }, { sku: 'Magnesium', quantity: 1, note: '' }]
    },
    select2: {
      options: ['3-6'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      let result;
      if (_.includes(this.select1.options, answers)) {
        result = this.select1.result;
      }
      if (_.includes(this.select2.options, answers)) {
        result = this.select2.result;
      }
      return result;
    }
  },
  {
    id: 'fJKlNvODJmO5',
    title: 'Are these drinks mostly taken in one night?',
    ref: '55357d7b-9602-495a-9939-50850c26c037',
    select: {
      options: [true],
      result: [{ sku: 'B complex', quantity: 1, note: '' }, { sku: 'Vitamin C', quantity: 1, note: '' }, { sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'TJEgzAcILzqT',
    title: 'Do you smoke?',
    ref: '030a3c01-4d43-4596-bd4e-e1df8bedc43d',
    select: {
      options: [true],
      result: [{ sku: 'Fish Oil', quantity: 1, note: 'Fish Oil or Omega 3 (vegan)' }, { sku: 'Vitamin C', quantity: 1, note: '' }, { sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'SwGX84aqWueU',
    title: 'Would you like to explore any of the below?',
    ref: '9fa094c2-45b5-448d-8f53-1e3a8627f338',
    select1: {
      options: ['Increase my performance'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    select2: {
      options: ['Improve my energy levels'],
      result: [{ sku: 'Vitamin B12', quantity: 1, note: '' }]
    },
    select3: {
      options: ['Support my weight loss'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }, { sku: 'Probiotics', quantity: 1, note: '' }, { sku: 'B complex', quantity: 1, note: '' }]
    },
    select4: {
      options: ['Strengthen my immune system'],
      result: [{ sku: 'Zinc', quantity: 1, note: '' }, { sku: 'Vitamin C', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      let result;
      if (_.includes(this.select1.options, answers)) {
        result = this.select1.result;
      }
      if (_.includes(this.select2.options, answers)) {
        result = this.select2.result;
      }
      if (_.includes(this.select3.options, answers)) {
        result = this.select3.result;
      }
      if (_.includes(this.select4.options, answers)) {
        result = this.select4.result;
      }
      return result;
    }
  },
  {
    id: 'R8JnUob1Waxt',
    title: 'What about your hair?',
    ref: '13723a2d-f15f-4245-94d4-7fc9dfbd93ea',
    select: {
      options: ['Dryness', 'Thinning', 'More than usual hair loss'],
      result: [{ sku: 'B complex', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  // {
  //   id: 'RztjrDur4kwV',
  //   title: 'Do any of these digestive health concerns relate to you?',
  //   ref: '813deb37-2804-46b6-8dd5-7824581d5b6c',
  //   select: {
  //     options: ['Bloating', 'Bowel movement irregularity'],
  //     result: [{ sku: 'Probiotics', quantity: 1, note: '' }]
  //   },
  //   actions: function(answers) {
  //     let result = [];
  //     for (const item of answers) {
  //       if (_.includes(this.select.options, item)) {
  //         result = [...result, ...this.select.result];
  //       }
  //     }
  //     return result;
  //   }
  // },
  {
    id: 'f0HUoRZYRLU1',
    title: 'What about your skin?',
    ref: '2e08df53-8c75-4402-b7d8-b8ebb331dedc',
    select1: {
      options: ['General ageing', 'Dryness', 'Wrinkles'],
      result: [{ sku: 'Zinc', quantity: 1, note: '' }, { sku: 'Vitamin C', quantity: 1, note: '' }]
    },
    select2: {
      options: ['Stubborn circles under your eyes'],
      result: [{ sku: 'B complex', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select1.options, answers) ? this.select1.result : this.select2.result;
    }
  },
  {
    id: 'paml5lAzVLAd',
    title: 'Is your bowel movement:',
    ref: 'd5981ee2-668e-4aec-b7de-85653762c146',
    select1: {
      options: ['Too much'],
      result: [{ sku: 'Probiotics', quantity: 1, note: '' }]
    },
    select2: {
      options: ['Not enough'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      let result = [];
      if (_.includes(this.select1.options, answers)) {
        result = [...result, this.select1.result];
      }
      if (_.includes(this.select2.options, answers)) {
        result = [...result, this.select2.result];
      }
      return result;
    }
  },
  {
    id: 'RztjrDur4kwV',
    title: 'Do any of these digestive health concerns relate to you?',
    ref: '813deb37-2804-46b6-8dd5-7824581d5b6c',
    select1: {
      options: ['Bloating', 'Bowel movement irregularity'],
      result: [{ sku: 'Probiotics', quantity: 1, note: '' }]
    },
    select2: {
      options: ['Reflux'],
      result: [{ sku: 'Digestive Enzymes', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      let result = [];
      for (const item of answers) {
        if ((_.includes(this.select1.options), item)) {
          result = [...result, ...this.select1.result];
        }
        if ((_.includes(this.select2.options), item)) {
          result = [...result, ...this.select2.result];
        }
      }
      return result;
    }
  },
  {
    id: 'TjEIwJeYt0Gs',
    title: 'What is your stress level out of 5?',
    ref: 'd66c02f7-df7f-4127-8b5f-4f40dd528507',
    select: {
      options: ['3', '4', '5'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'eB3Jlgg49lP8',
    title: 'How often do you feel stressed?',
    ref: '0ea9b39a-78b4-41ce-a62c-eac610974e86',
    select: {
      options: ['Frequently: almost every day'],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }, { sku: 'Zinc', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'iPcbZYtHrcB7',
    title: 'Do you take any medication to manage your stress levels?',
    ref: '62a461e6-c80c-4b07-853c-79a0494dbe31',
    select: {
      options: [true],
      result: function(array) {
        // array = [];
        return array;
      }
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'yl5FF3vpQyHF',
    title: 'How many hours of sleep do you get on average per night?',
    ref: 'd4012f3b-e4fd-46d9-b86b-26577ba5a2c6',
    select: {
      options: [],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return parseInt(answers) < 7 ? this.select.result : null;
    }
  },
  {
    id: 'zsclphmc1xeD',
    title: 'Do you wake up during the night?',
    ref: '456b39d9-89e1-47c9-976d-6eda454a7f36',
    select: {
      options: [true],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'lHqdlnbLnxU1',
    title: 'Do you wake up in the morning feeling tired?',
    select: {
      options: [true],
      result: [{ sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'epBHhvzqdRZf',
    title: 'Do you use sleeping pills?',
    select: {
      options: [true],
      result: function(array) {
        // array = [];
        return array;
      }
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  },
  {
    id: 'eMdvZvyWig5F',
    title: 'How often?',
    ref: '39e96164-48b9-4055-a03f-8dc101491f73',
    select: {
      options: ['Weekly'],
      result: [{ sku: 'B complex', quantity: 1, note: '' }, { sku: 'Vitamin C', quantity: 1, note: '' }, { sku: 'Magnesium', quantity: 1, note: '' }]
    },
    actions: function(answers) {
      return _.includes(this.select.options, answers) ? this.select.result : null;
    }
  }
];

module.exports = Questions;
