import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const CategoryProduct = sequelize.define(
  'category_product',
  {
    product_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        table: 'product',
        field: 'id'
      }
    },
    category_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        table: 'category',
        field: 'id'
      }
    }
  },
  {
    tableName: 'category_product',
    underscored: true,
    paranoid: false
  }
);

export default CategoryProduct;
