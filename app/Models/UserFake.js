import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import User from './User';

const UserFake = sequelize.define(
  'user_fake',
  {
    user_id: {
      type: Sequelize.INTEGER
    },
    user_fake: {
      type: Sequelize.STRING
    }
  },
  {
    tableName: 'user_fake',
    underscored: true,
    paranoid: false
  }
);

UserFake.belongsTo(User);
UserFake.addScope('getUser', {
  include: [{ model: User, attributes: ['email', 'status'] }]
});
export default UserFake;
