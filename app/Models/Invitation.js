import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Invitation = sequelize.define(
  'invitation',
  {
    email: {
      type: Sequelize.STRING
    }
  },
  {
    underscored: true,
    paranoid: false
  }
);
Invitation.prototype.notify = function(notification) {
  const _this = this;
  notification.setNotifiable(_this);
  notification.execute();
};
export default Invitation;
