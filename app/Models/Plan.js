import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Plan = sequelize.define(
  'plan',
  {
    interval: {
      type: Sequelize.STRING
    },
    nickname: {
      type: Sequelize.STRING
    },
    currency: {
      type: Sequelize.STRING
    },
    amount: {
      type: Sequelize.FLOAT
    },
    plan_id: {
      type: Sequelize.STRING
    }
  },
  {
    tableName: 'plans',
    underscored: true,
    paranoid: false
  }
);

export default Plan;
