import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import Category from './Category';
import Image from './Image';

const Product = sequelize.define(
  'product',
  {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    sku: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    description: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    short_description: {
      type: Sequelize.STRING
    },
    price: {
      type: Sequelize.FLOAT,
      allowNull: false
    },
    status: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    quantity: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    color: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    benefit: {
      type: Sequelize.TEXT
    },
    research: {
      type: Sequelize.TEXT
    },
    formula: {
      type: Sequelize.TEXT
    },
    weight: {
      type: Sequelize.FLOAT
    }
  },
  {
    tableName: 'products',
    underscored: true,
    paranoid: false
  }
);

Product.belongsToMany(Category, { through: 'category_product' });

Product.hasMany(Image, {
  foreignKey: 'imageable_id',
  // as: 'images',
  scope: {
    imageable_type: 'product'
  }
});

Product.addScope(
  'getQuantity',
  {
    attributes: ['id', 'name', 'quantity']
  },
  { override: true }
);

export default Product;
