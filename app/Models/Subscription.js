import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import Plan from './Plan';

const User = sequelize.define(
  'user',
  {
    email: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    password: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    last_login: {
      type: Sequelize.DATE
    }
  },
  {
    tableName: 'users',
    underscored: true,
    paranoid: false
  }
);

const Subscription = sequelize.define(
  'subscription',
  {
    user_id: {
      type: Sequelize.INTEGER
    },
    plan_id: {
      type: Sequelize.INTEGER
    },
    start_time: {
      type: Sequelize.DATE
    },
    subscription_id: {
      type: Sequelize.STRING
    },
    status : {
      type: Sequelize.STRING
    }
  },
  {
    underscored: true,
    paranoid: false
  }
);

Subscription.belongsTo(User);
Subscription.belongsTo(Plan);

export default Subscription;
