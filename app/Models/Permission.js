import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Permission = sequelize.define(
  'permission',
  {
    name: Sequelize.STRING,
    slug: Sequelize.STRING,
    group_id: {
      type: Sequelize.INTEGER
    }
  },
  {
    tableName: 'permissions',
    underscored: true
  }
);

export default Permission;
