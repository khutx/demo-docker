import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import OrderItem from './OrderItem';
import Product from './Product';

export const draft = 1;
export const open = 2;
export const paid = 3;
export const uncollectible = 4;

const User = sequelize.define(
  'user',
  {
    email: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    password: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    last_login: {
      type: Sequelize.DATE
    }
  },
  {
    tableName: 'users',
    underscored: true,
    paranoid: false
  }
);

const Order = sequelize.define(
  'order',
  {
    user_id: {
      type: Sequelize.INTEGER,
      references: {
        table: 'user',
        field: 'id'
      }
    },
    payment_method: {
      type: Sequelize.STRING
    },
    amount: {
      type: Sequelize.FLOAT
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    next_payment_attempt: {
      allowNull: false,
      type: Sequelize.STRING
    },
    note: {
      type: Sequelize.STRING
    },
    shipping_method_id: {
      type: Sequelize.STRING
    }
  },
  {
    tableName: 'orders',
    underscored: true,
    paranoid: false
  }
);
Order.belongsTo(User);
Order.belongsToMany(Product, { through: OrderItem });
// Order.addHook('afterCreate', function(item) {
//   console.log(item);
// });
export default Order;
