import sequelize from '../../config/sequelize';
import Sequelize from 'sequelize';
import Permission from './Permission';

const PermissionGroup = sequelize.define(
  'PermissionGroup',
  {
    name: Sequelize.STRING,
    slug: Sequelize.STRING
  },
  {
    underscored: true,
    tableName: 'permission_groups'
  }
);

PermissionGroup.hasMany(Permission, {
  foreignKey: 'group_id'
});

export default PermissionGroup;
