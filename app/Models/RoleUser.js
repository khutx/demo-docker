import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const RoleUser = sequelize.define(
  'role_user',
  {
    user_id: {
      type: Sequelize.INTEGER
    },
    role_id: {
      type: Sequelize.INTEGER
    }
  },
  {
    tableName: 'role_user',
    underscored: true,
    paranoid: false
  }
);

export default RoleUser;
