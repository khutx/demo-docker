import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import Role from './Role';
import _ from 'lodash';
import jwt from 'jsonwebtoken';
import PasswordResets from './PasswordResets';
import Image from './Image';
import Recommendation from './Recommendation';
import Order from './Order';
import Product from './Product';
import Address from './Address';
import Subscription from './Subscription';
import Customer from './Customer';
import RoleUser from './RoleUser';
import Plan from './Plan';

export const PENDING_USER_STATUS = 1;

const User = sequelize.define(
  'user',
  {
    email: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    password: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    last_login: {
      type: Sequelize.DATE
    }
  },
  {
    tableName: 'users',
    underscored: true,
    paranoid: false
  }
);
User.hasMany(Image, {
  foreignKey: 'imageable_id'
});

User.hasOne(Customer);
User.belongsTo(PasswordResets, { foreignKey: 'email', targetKey: 'email' });
User.hasMany(Recommendation);

User.hasMany(Order);
User.hasOne(Subscription);
User.belongsToMany(Role, { through: RoleUser });
User.hasMany(Address);
User.addScope('getRecommendation', {
  include: [{ model: Recommendation }]
});

User.addScope('getBilling', {
  include: [{ model: Customer, attributes: ['card_last_four', 'card_expire', 'referal_code'] }]
});
// User.addScope('getRecommendationById', id => {
//   return {
//     include: [{ model: Recommendation, where: { id: id }, include: [{ model: Product }] }]
//   };
// });

User.addScope('getSubscription', {
  include: [{ model: Subscription, include: [{ model: Plan }] }]
});

User.addScope('getOrder', {
  include: [{ model: Order, include: [{ model: Product }] }]
});
// User.addScope('getOrder', (page, per_page) => {
//   return {
//     include: [
//       {
//         model: Order,
//         offset: (page - 1) * per_page,
//         limit: per_page,
//         required: true,
//         include: [{ model: Product }]
//       }
//     ]
//   };
// });

User.prototype.can = async function(permission) {
  const roles = this.roles;
  if (!_.isUndefined(_.find(roles, item => item.isSuperAdmin()))) {
    return true;
  }
  let hasPermission = false;
  _.forEach(roles, item => {
    if (typeof item.permissions === 'string') {
      hasPermission = item.permissions.indexOf(permission) > -1;
    } else {
      if (_.includes(item.permissions, permission)) {
        hasPermission = true;
      }
    }
  });
  return hasPermission;
};
User.prototype.isRole = function(slug) {
  return new Promise(resolve => {
    if (_.isArray(this.roles) && this.roles.length > 0) {
      resolve(!_.isUndefined(_.find(this.roles, item => item.slug === slug)));
    } else {
      User.findOne({
        where: { id: this.id },
        include: { model: Role, where: { slug: slug } }
      })
        .then(result => {
          if (!_.isNil(result)) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch(() => {
          resolve(false);
        });
    }
  });
};

User.prototype.resetPasstoken = function(token = '') {
  this.resetPasswordToken = token;
};

User.prototype.resetEmailtoken = function(token = '') {
  this.resetEmailToken = token;
  return this;
};

User.prototype.notify = function(notification) {
  const _this = this;
  notification.setNotifiable(_this);
  notification.execute();
};

User.prototype.generateToken = function() {
  const data = _.pick(this, ['id', 'email', 'status']);
  return jwt.sign({ data }, process.env.JWT_SECRET, { expiresIn: 20160000 });
};

export default User;
