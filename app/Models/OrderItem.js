import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import Product from './Product';

const OrderItem = sequelize.define(
  'order_item',
  {
    order_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        table: 'order',
        field: 'id'
      }
    },
    product_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        table: 'product',
        field: 'id'
      }
    },
    quantity: {
      type: Sequelize.INTEGER
    },
    note: {
      type: Sequelize.STRING
    }
  },
  {
    underscored: true,
    paranoid: false,
    tableName: 'order_items'
  }
);
OrderItem.belongsTo(Product);
export default OrderItem;
