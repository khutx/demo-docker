import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Category = sequelize.define(
  'category',
  {
    name: {
      type: Sequelize.STRING
    },
    slug: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    parent_id: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    image: {
      type: Sequelize.STRING
    }
  },
  {
    tableName: 'categories',
    underscored: true,
    paranoid: false
  }
);

export default Category;
