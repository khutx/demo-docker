import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const PasswordResets = sequelize.define(
  'password_resets',
  {
    email: {
      type: Sequelize.STRING
    },
    token: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    }
  },
  {
    tableName: 'password_resets',
    underscored: true,
    paranoid: false
  }
);

export default PasswordResets;
