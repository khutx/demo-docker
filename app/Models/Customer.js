import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import * as DB_CONST from '../../config/const';

const User = sequelize.define(
  'user',
  {
    email: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    password: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },
    last_login: {
      type: Sequelize.DATE
    }
  },
  {
    tableName: 'users',
    underscored: true,
    paranoid: false
  }
);

const Customer = sequelize.define(
  'customer',
  {
    user_id: {
      type: Sequelize.INTEGER
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    birth: {
      type: Sequelize.DATE
    },
    gender: {
      type: Sequelize.ENUM,
      values: DB_CONST.GENDER
    },
    phone: {
      type: Sequelize.STRING
    },
    phone_contact: {
      type: Sequelize.STRING
    },
    address: {
      type: Sequelize.STRING
    },
    shipping_address: {
      type: Sequelize.STRING
    },
    group: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    payment_id: {
      type: Sequelize.STRING
    },
    card_last_four: {
      type: Sequelize.STRING
    },
    card_brand: {
      type: Sequelize.STRING
    },
    card_expire: {
      type: Sequelize.STRING
    },
    referal_code: {
      type: Sequelize.STRING
    }
  },
  {
    tableName: 'customers',
    underscored: true,
    paranoid: false
  }
);

Customer.belongsTo(User);

export default Customer;
