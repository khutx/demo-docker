import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Email = sequelize.define(
  'email',
  {
    email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.INTEGER
    }
  },
  {
    tableName: 'emails',
    underscored: true,
    paranoid: false
  }
);

export default Email;
