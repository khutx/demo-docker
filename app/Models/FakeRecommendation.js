import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import Product from './Product';
import Image from './Image';
export const PENDING_UPDATE = 1;
export const NO_PENDING_UPDATE = 0;

const FakeRecommendation = sequelize.define(
  'fake_recommendation',
  {
    user_id: {
      type: Sequelize.STRING
    },
    product_id: {
      type: Sequelize.INTEGER
    },
    quantity: {
      type: Sequelize.INTEGER
    },
    note: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.INTEGER,
      defaultValue: PENDING_UPDATE
    }
  },
  {
    underscored: true,
    paranoid: false
  }
);

FakeRecommendation.belongsTo(Product);

FakeRecommendation.addScope('getProduct', {
  include: [{ model: Product, include: [{ model: Image, where: { imageable_type: 'product' } }] }]
});

export default FakeRecommendation;
