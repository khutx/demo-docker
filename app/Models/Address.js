import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Address = sequelize.define(
  'address',
  {
    user_id: {
      type: Sequelize.INTEGER
    },
    company: {
      type: Sequelize.STRING
    },
    address_line_1: {
      allowNull: false,
      type: Sequelize.STRING
    },
    address_line_2: {
      allowNull: false,
      type: Sequelize.STRING
    },
    city: {
      allowNull: false,
      type: Sequelize.STRING
    },
    state: {
      allowNull: false,
      type: Sequelize.STRING
    },
    country: {
      allowNull: false,
      type: Sequelize.STRING
    },
    zipcode: {
      allowNull: false,
      type: Sequelize.STRING
    },
    phone: {
      allowNull: false,
      type: Sequelize.STRING
    },
    is_default: {
      type: Sequelize.BOOLEAN
    },
    is_default_billing: {
      type: Sequelize.BOOLEAN
    }
  },
  {
    tableName: 'addresses',
    underscored: true,
    paranoid: false
  }
);

export default Address;
