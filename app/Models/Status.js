import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Status = sequelize.define(
  'status',
  {
    name: {
      type: Sequelize.STRING
    },
    slug: {
      type: Sequelize.STRING,
      defaultValue: ''
    }
  },
  {
    tableName: 'status',
    underscored: true,
    paranoid: false
  }
);

export default Status;
