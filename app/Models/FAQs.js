import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const FAQs = sequelize.define(
  'faqs',
  {
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },
  {
    tableName: 'faqs',
    underscored: true,
    paranoid: false
  }
);

export default FAQs;
