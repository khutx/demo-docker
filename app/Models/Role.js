import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';
import User from './User';
import RoleUser from './RoleUser';
import _ from 'lodash';

const Role = sequelize.define(
  'role',
  {
    name: {
      type: Sequelize.STRING,
      set(val) {
        this.setDataValue('name', _.lowerCase(val));
        this.setDataValue('slug', _.snakeCase(val));
      }
    },
    permissions: {
      type: Sequelize.JSON,
      get(val) {
        return JSON.parse(this.getDataValue(val));
      },
      set(val) {
        if (!_.isArray(val)) {
          val = [];
        }
        this.setDataValue('permissions', JSON.stringify(val));
      }
    },
    slug: {
      type: Sequelize.STRING,
      defaultValue: ''
    },
    level: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    }
  },
  {
    underscored: true
  }
);
Role.associate = () => {
  Role.belongsToMany(User, { through: RoleUser });
};

Role.prototype.showUser = function() {
  const list_user = {};
  _.forEach(this.users, item => {
    list_user['user'] = item.email;
  });
  return list_user;
};

Role.prototype.isSuperAdmin = function() {
  return this.slug === 'superadmin';
};

Role.prototype.isCustomer = function() {
  return this.slug === 'customer';
};

export default Role;
