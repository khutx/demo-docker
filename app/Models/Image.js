import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

const Image = sequelize.define(
  'image',
  {
    imageable_id: {
      type: Sequelize.INTEGER
    },
    imageable_type: {
      type: Sequelize.STRING
    },
    url: {
      type: Sequelize.STRING
    },
    type: {
      type: Sequelize.STRING
    }
  },
  {
    underscored: true,
    paranoid: false
  }
);

export default Image;
