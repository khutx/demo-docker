import Sequelize from 'sequelize';
import sequelize from '../../config/sequelize';

import * as DB_CONST from '../../config/const';

const PaymentMethod = sequelize.define(
  'payment_methods',
  {
    name: {
      type: Sequelize.STRING
    },
    availability: {
      type: Sequelize.ENUM,
      values: DB_CONST.PAYMENT_TYPE
    },
    required_value: {
      type: Sequelize.TEXT
    }
  },
  {
    tableName: 'payment_methods',
    underscored: true,
    paranoid: false
  }
);

export default PaymentMethod;
