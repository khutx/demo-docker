import { Notification, MAIL, SMS } from './Notification';
import CustomEmail from '../Emails/CustomEmail';

export class SendForgotPasswordNotification extends Notification {
  via() {
    return [MAIL, SMS];
  }

  toMail(notifiable) {
    const url = process.env.APP_URL + '/auth/reset-password';

    return new CustomEmail()
      .to(notifiable.email)
      .subject('Forgot password')
      .greeting(`Hi ${notifiable.email}`)
      .line('Comfirm email:')
      .line(`<a href="${url}?token=${notifiable.resetPasswordToken}">${url}?token=${notifiable.resetPasswordToken}</a>`)
      .action('Button', `${url}?token=${notifiable.resetPasswordToken}`)
      .line('Thank for using application!');
  }
}
