import { Notification, MAIL } from './Notification';
import CustomEmail from '../Emails/CustomEmail';

export class SendThanksEmailNotification extends Notification {
  via() {
    return [MAIL];
  }

  toMail(notifiable) {
    return (
      new CustomEmail()
        .to(notifiable.email)
        .subject('wellcome')
        .greeting(`Hello !`)
        .line('Thank you !')
        .action('Button', 'https://google.com')
        .line('At the end is a line of text')
    );
  }
}
