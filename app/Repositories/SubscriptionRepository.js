import Subscription from '../Models/Subscription';
import { Repository } from '@nsilly/repository';

export default class SubscriptionRepository extends Repository {
  Models() {
    return Subscription;
  }
}
