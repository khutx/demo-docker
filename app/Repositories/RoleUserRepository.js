import { Repository } from './Repository';
import RoleUser from '../Models/RoleUser';

export default class RoleUserRepository extends Repository {
  Models() {
    return RoleUser;
  }
}
