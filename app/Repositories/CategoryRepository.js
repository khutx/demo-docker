import { Repository } from './Repository';
import Category from '../Models/Category';
import Product from '../Models/Product';

export default class CategoryRepository extends Repository {
  Models() {
    Category.belongsToMany(Product, { through: 'category_product' });
    return Category;
  }
}
