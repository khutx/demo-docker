import { Repository } from './Repository';
import PermissionGroup from '../Models/PermissionGroup';

export default class PermissionGroupRepository extends Repository {
  Models() {
    return PermissionGroup;
  }
}
