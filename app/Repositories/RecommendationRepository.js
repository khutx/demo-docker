import { Repository } from './Repository';
import Recommendation from '../Models/Recommendation';

export default class RecommendationRepository extends Repository {
  Models() {
    return Recommendation;
  }
}
