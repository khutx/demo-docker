import { Repository } from './Repository';
import CategoryProduct from '../Models/CategoryProduct';

export default class CategoryProductRepository extends Repository {
  Models() {
    return CategoryProduct;
  }
}
