import { Repository } from './Repository';
import Address from '../Models/Address';

export default class AddressRepository extends Repository {
  Models() {
    return Address;
  }
}
