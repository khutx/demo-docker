import { Repository } from './Repository';
import Permission from '../Models/Permission';

export default class PermissionRepository extends Repository {
  Models() {
    return Permission;
  }

  async addPermission(newpermission) {
    this.newpermission = newpermission;
    const created = await Permission.create(this.newpermission);
    return created;
  }
}
