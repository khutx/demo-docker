import { Repository } from './Repository';
import Invitation from '../Models/Invitation';

export default class InvitationRepository extends Repository {
  Models() {
    return Invitation;
  }
}
