import { Repository } from './Repository';
import FAQs from '../Models/FAQs';

export default class FAQsRepository extends Repository {
  Models() {
    return FAQs;
  }
}
