import { Repository } from './Repository';
import Role from '../Models/Role';

export default class RoleRepository extends Repository {
  Models() {
    return Role;
  }
}
