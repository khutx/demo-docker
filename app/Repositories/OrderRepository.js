import { Repository } from './Repository';
import Order from '../Models/Order';

export default class OrderRepository extends Repository {
  Models() {
    return Order;
  }
}
