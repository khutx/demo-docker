import { Repository } from './Repository';
import OrderItem from '../Models/OrderItem';

export default class OrderItemRepository extends Repository {
  Models() {
    return OrderItem;
  }
}
