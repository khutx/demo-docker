import { Repository } from './Repository';
import Product from '../Models/Product';

export default class ProductRepository extends Repository {
  Models() {
    return Product;
  }
}
