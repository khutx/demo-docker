import { Repository } from './Repository';
import Plan from '../Models/Plan';

export default class PlanRepository extends Repository {
  Models() {
    return Plan;
  }
}
