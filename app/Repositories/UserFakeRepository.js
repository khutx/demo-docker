import { Repository } from './Repository';
import UserFake from '../Models/UserFake';

export default class UserFakeRepository extends Repository {
  Models() {
    return UserFake;
  }
}
