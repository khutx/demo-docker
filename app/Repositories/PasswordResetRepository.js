import { Repository } from './Repository';
import PasswordResets from '../Models/PasswordResets';

export default class PasswordResetRepository extends Repository {
  Models() {
    return PasswordResets;
  }
}
