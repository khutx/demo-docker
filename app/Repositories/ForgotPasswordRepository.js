import { Repository } from './Repository';
import ForgotPassword from '../Models/ForgotPassword';

export default class ForgotPasswordRepository extends Repository {
  Models() {
    return ForgotPassword;
  }
}
