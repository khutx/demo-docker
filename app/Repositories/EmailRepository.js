import { Repository } from './Repository';
import Email from '../Models/Email';

export default class EmailRepository extends Repository {
  Models() {
    return Email;
  }
}
