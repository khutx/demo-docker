import { Repository } from './Repository';
import Status from '../Models/Status';

export default class StatusRepository extends Repository {
  Models() {
    return Status;
  }
}
