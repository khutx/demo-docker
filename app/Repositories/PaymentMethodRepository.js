import { Repository } from './Repository';
import PaymentMethod from '../Models/PaymentMethod';

export default class PaymentMethodRepository extends Repository {
  Models() {
    return PaymentMethod;
  }
}
