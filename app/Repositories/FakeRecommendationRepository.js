import { Repository } from './Repository';
import FakeRecommendation from '../Models/FakeRecommendation';

export default class FakeRecommendationRepository extends Repository {
  Models() {
    return FakeRecommendation;
  }
}
