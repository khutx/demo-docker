import Image from '../Models/Image';
import { Repository } from '@nsilly/repository';

export default class ImageRepository extends Repository {
  Models() {
    return Image;
  }
}
