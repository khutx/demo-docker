import { Repository } from './Repository';
import Customer from '../Models/Customer';

export default class CustomerRepository extends Repository {
  Models() {
    return Customer;
  }
}
