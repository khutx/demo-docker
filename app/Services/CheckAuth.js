import { Auth } from '@nsilly/auth';
export class CheckAuth {
  check(id) {
    return Auth.id() === parseInt(id);
  }
}
