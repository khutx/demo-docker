import multer from 'multer';
import _ from 'lodash';
import multerS3 from 'multer-s3';
import aws from 'aws-sdk';
import path from 'path';
import { Auth } from '@nsilly/auth';

export class FileStorage {
  constructor() {
    if (process.env.MEDIA_STORAGE === 'local') {
      this.folder = 'public/uploads';
    } else {
      this.folder = 'public/uploads';
    }
    this.type = [];
    this.limit = 10;
  }

  destination(folder) {
    this.folder = folder;
    return this;
  }

  typeFile(file) {
    this.type.push(file);
    return this;
  }

  limitFileUpload(num) {
    this.limit = num;
    return this;
  }

  setUpMulter() {
    let storage;
    if (process.env.FILE_STORAGE === 's3') {
      storage = multerS3({
        s3: new aws.S3({
          accessKeyId: process.env.S3_ACCESS_KEY,
          secretAccessKey: process.env.S3_SECRET_KEY,
          region: process.env.S3_REGION
        }),
        bucket: process.env.S3_BUCKET_NAME,
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        cacheControl: 'max-age=31536000',
        key: function(res, file, cb) {
          if (Auth.check()) {
            cb(null, Auth.id() + '/' + _.kebabCase(file.originalname + Date.now()) + path.extname(file.originalname));
          } else {
            cb(null, _.kebabCase(file.originalname + Date.now()) + path.extname(file.originalname));
          }
        }
      });
    } else {
      storage = multer.diskStorage({
        destination: this.folder,
        filename: function(res, file, cb) {
          cb(null, _.kebabCase(file.originalname + Date.now()));
        }
      });
    }

    const upload = multer({
      storage: storage,
      fileFilter: (req, file, cb) => {
        const extension = file.mimetype.split('/')[0];
        if (this.type.indexOf(extension) === -1) {
          return cb(new Error('type not accepted'), false);
        }
        cb(null, true);
      }
    });

    return upload;
  }

  /**
   * Create or update a record matching the attributes, and fill it with values
   *
   * @param name string
   *
   * @return callback
   */

  uploadSingleFile(name) {
    return new Promise((resolve, reject) => {
      try {
        resolve(this.setUpMulter().single(name));
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Create or update a record matching the attributes, and fill it with values
   *
   * @param name string
   *
   * @return callback
   */
  uploadMultiFile(name) {
    return new Promise((resolve, reject) => {
      try {
        resolve(this.setUpMulter().array(name, this.limit));
      } catch (e) {
        reject(e);
      }
    });
  }
}
