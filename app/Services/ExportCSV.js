import fs from 'fs';
import aws from 'aws-sdk';
import downloadsFolder from 'downloads-folder';
const Json2CSVParse = require('json2csv').Parser;

export class ExprotCSV {
  exportFileCSV(query, fields, model, res) {
    const filename = `${Date.now()}_${model}.csv`;
    const json2csv = new Json2CSVParse({ fields: fields });
    const csv = json2csv.parse(query);
    var path = downloadsFolder() + '/' + filename;
    fs.writeFile(path, csv, function(err) {
      if (err) throw err;
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/csv');
      // res.setHeader('Content-Type', 'application/octet-stream');
      res.setHeader('Content-Disposition', 'attachment; filename=' + filename);
      res.download(path);
    });
  }

  pushS3Bucket(data, fields, model) {
    return new Promise((resolve, reject) => {
      try {
        const s3 = new aws.S3({
          accessKeyId: process.env.S3_ACCESS_KEY,
          secretAccessKey: process.env.S3_SECRET_KEY,
          region: process.env.S3_REGION
        });
        const json2csv = new Json2CSVParse({ fields: fields });
        const csv = json2csv.parse(data);
        const options = { partSize: 10 * 1024 * 1024, queueSize: 1 };
        const params = {
          Bucket: process.env.S3_BUCKET_NAME,
          Body: Buffer.from(csv, 'utf8'),
          Key: `${model}_${Date.now()}.csv`,
          ContentType: 'text/csv',
          ContentEncoding: 'utf-8',
          ACL: 'public-read'
        };

        s3.upload(params, options, (error, data) => {
          if (!error) {
            resolve(data);
          } else {
            reject(error);
          }
        });
      } catch (e) {
        reject(e);
      }
    });
  }
}
