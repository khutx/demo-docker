export class Invoice {
  constructor(data) {
    this.id = data.id;
    this.amount_due = data.amount_due;
    this.amount_paid = data.amount_paid;
    this.amount_remaining = data.amount_remaining;
    this.application_fee = data.application_fee;
    this.attempt_count = data.attempt_count;
    this.attempted = data.attempted;
    this.billing = data.billing;
    this.billing_reason = data.billing_reason;
    this.charge = data.charge;
    this.currency = data.currency;
    this.date = data.date;
    this.default_source = data.default_source;
    this.description = data.description;
    this.discount = data.discount;
    this.ending_balance = data.ending_balance;
    this.finalized_at = data.finalized_at;
  }
  setRaw(data) {
    this.raw = data;
    return this;
  }
  isCanceled() {
    return this.canceled_at !== null || this.canceled_at !== undefined;
  }
}
