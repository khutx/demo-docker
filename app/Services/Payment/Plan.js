export class Plan {
  constructor(data) {
    this.id = data.id;
    this.active = data.active;
    this.aggregate_usage = data.aggregate_usage;
    this.amount = data.amount;
    this.billing_scheme = data.billing_scheme;
    this.created = data.created;
    this.currency = data.currency;
    this.interval = data.interval;
    this.interval_count = data.interval_count;
    this.livemode = data.livemode;
    this.metadata = data.metadata;
    this.nickname = data.nickname;
    this.product = data.product;
    this.tiers = data.tiers;
    this.tiers_mode = data.tiers_mode;
    this.transform_usage = data.transform_usage;
    this.trial_period_days = data.trial_period_days;
    this.usage_type = data.usage_type;
  }
  setRaw(data) {
    this.raw = data;
    return this;
  }
}
