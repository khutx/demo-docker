import StripeSdk from 'stripe';
import { Customer } from '../../Customer';
import _ from 'lodash';
import { Plan } from '../../Plan';
import { Subscription } from '../../Subscription';
import { AbstractGateway } from '../AbstractGateway';
// import { Invoice } from '../../Invoice';

export class Stripe extends AbstractGateway {
  constructor() {
    super();
    this.stripe = new StripeSdk(process.env.STRIPE_SECRET_KEY);
  }

  /**
   * List all customers
   *
   * @return Array
   */
  async listCustomers(limit = 3) {
    const result = await this.stripe.customers.list({ limit });
    const customers = _.map(result.data, item => {
      const cus = new Customer(item);
      cus.setRaw(item);
      return cus;
    });
    return customers;
  }

  /**
   * Create new customer
   *
   * @param Object
   * @return Customer
   */
  async createCustomer(data) {
    const result = await this.stripe.customers.create(data);
    const customer = new Customer(result);
    customer.setRaw(result);
    return customer;
  }

  /**
   * Retrieve a customer
   *
   * @param id
   * @return Customer
   */
  async getCustomerDetail(customer_id) {
    const result = await this.stripe.customers.retrieve(customer_id);
    const customer = new Customer(result);
    customer.setRaw(result);
    return customer;
  }

  /**
   * Update customer information
   *
   * @param customer_id
   * @param data
   * @return Customer
   */
  async updateCustomer(customer_id, data) {
    const result = await this.stripe.customers.update(customer_id, data);
    const customer = new Customer(result);
    customer.setRaw(result);
    return customer;
  }

  /**
   * Delete a customer
   *
   * @param id
   * @return Boolean
   */
  async deleteCustomer(customer_id) {
    const result = await this.stripe.customers.del(customer_id);
    return result.deleted;
  }

  /**
   * Plan API
   *
   * Plans define the base price, currency, and billing cycle for subscriptions.
   */

  /**
   * List all plans
   *
   * @return Plan[]
   */
  async listPlans(limit) {
    const result = await this.stripe.plans.list({ limit });
    const plans = _.map(result.data, item => {
      const plan = new Plan(item);
      plan.setRaw(item);
      return plan;
    });
    return plans;
  }

  /**
   * Create new plan
   *
   * @param Object
   * @return Plan
   */
  async createPlan(data) {
    const result = await this.stripe.plans.create(data);
    const plan = new Plan(result);
    plan.setRaw(result);
    return plan;
  }
  /**
   * Retreive a plan
   *
   * @param plan_id
   * @return Plan
   */
  async getPlanDetail(plan_id) {
    const result = await this.stripe.plans.retrieve(plan_id);
    const plan = new Plan(result);
    plan.setRaw(result);
    return plan;
  }

  /**
   * Delete a plan
   *
   * @param id
   * @return Boolean
   */
  async deletePlan(plan_id) {
    const result = await this.stripe.plans.del(plan_id);
    return result.deleted;
  }

  /**
   * Subscription API
   *
   * Subscriptions allow you to charge a customer on a recurring basis. A subscription ties a customer to a particular plan you've created.
   */

  /**
   * List subscriptions
   *
   * @param limit
   * @return Subscription[]
   */
  async listSubscriptions(limit = 10) {
    const result = await this.stripe.subscriptions.list({ limit });
    const subscriptions = _.map(result.data, item => {
      const subscription = new Subscription(item);
      subscription.setRaw(item);
      return subscription;
    });
    return subscriptions;
  }

  /**
   * Create a subscription
   *
   * @param data
   * @return Subscription
   */
  async subscribe(data) {
    const result = await this.stripe.subscriptions.create({
      customer: data.customer_id,
      items: [
        {
          plan: data.plan
        }
      ]
    });
    const subscription = new Subscription(result);
    subscription.setRaw(result);
    return subscription;
  }

  /**
   * Retreive a subscription
   *
   * @param subscription_id
   * @return Subscription
   */
  async getSubscriptionDetail(subscription_id) {
    const result = await this.stripe.subscriptions.retrieve(subscription_id);
    const subscription = new Subscription(result);
    subscription.setRaw(result);
    return subscription;
  }

  /**
   * Cancel a subscription
   *
   * @param subscription_id
   * @return Subscription
   */
  async cancelSubscription(subscription_id) {
    const result = await this.stripe.subscriptions.del(subscription_id);
    const subscription = new Subscription(result);
    subscription.setRaw(result);
    return subscription;
  }

  /**
   * Get Sources Customer
   *
   * @param customer_id
   * @return Sources
   */
  async getSourceCustomer(customer_id) {
    const source = await this.stripe.customers.listSources(customer_id);
    const length = source.data.length;
    const sources = source.data[length - 1];
    return sources;
  }

  /**
   * Update subscription information
   *
   * @param subscription_id
   * @param data
   * @return Subscription
   */
  async updateSubscription(subscription_id, data) {
    const result = await this.stripe.subscriptions.update(subscription_id, data);
    const subscription = new Subscription(result);
    subscription.setRaw(result);
    return subscription;
  }

  async getInvoice(customer_id) {
    const result = await this.stripe.invoices.retrieveUpcoming(customer_id);
    // const invoice = new Invoice(result);
    // invoice.setRaw(result);
    return result;
  }
}
