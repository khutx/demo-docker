import { Exception } from '@nsilly/exceptions';
export class AbstractGateway {
  constructor() {
    this.version = '1.0';
  }

  /**
   * List all customers
   *
   * @param int
   * @return Array Customer
   */
  async listCustomers(limit) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Create new customer
   *
   * @param Object
   * @return Customer
   */
  async createCustomer(data) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Retrieve a customer
   *
   * @param id
   * @return Customer
   */
  async getCustomerDetail(customer_id) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Update customer information
   *
   * @param customer_id
   * @param data
   * @return Customer
   */
  async updateCustomer(customer_id, data) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Delete a customer
   *
   * @param id
   * @return Boolean
   */
  async deleteCustomer(customer_id) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Plan API
   *
   * Plans define the base price, currency, and billing cycle for subscriptions.
   */

  /**
   * List all plans
   *
   * @param int
   * @return Plan[]
   */
  async listPlans(limit) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Create new plan
   *
   * @param Object
   * @return Plan
   */
  async createPlan(data) {
    throw new Exception('Method is not implemented', 1);
  }
  /**
   * Retreive a plan
   *
   * @param id
   * @return Plan
   */
  async getPlanDetail(plan_id) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Delete a plan
   *
   * @param id
   * @return Boolean
   */
  async deletePlan(id) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Subscription API
   *
   * Subscriptions allow you to charge a customer on a recurring basis. A subscription ties a customer to a particular plan you've created.
   */

  /**
   * List subscriptions
   *
   * @param limit
   * @return Subscription[]
   */
  async listSubscriptions(limit = 10) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Create a subscription
   *
   * @param data
   * @return Subscription
   */
  async subscribe(data) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Retreive a subscription
   *
   * @param subscription_id
   * @return Subscription
   */
  async getSubscriptionDetail(subscription_id) {
    throw new Exception('Method is not implemented', 1);
  }

  /**
   * Cancel a subscription
   *
   * @param subscription_id
   * @return Subscription
   */
  async cancelSubscription(subscription_id) {
    throw new Exception('Method is not implemented', 1);
  }
}
