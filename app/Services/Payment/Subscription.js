export class Subscription {
  constructor(data) {
    this.id = data.id;
    this.object = data.object;
    this.application_fee_percent = data.application_fee_percent;
    this.billing = data.billing;
    this.billing_cycle_anchor = data.billing_cycle_anchor;
    this.cancel_at_period_end = data.cancel_at_period_end;
    this.canceled_at = data.canceled_at;
    this.created = data.created;
    this.current_period_end = data.current_period_end;
    this.current_period_start = data.current_period_start;
    this.customer = data.customer;
    this.days_until_due = data.days_until_due;
    this.discount = data.discount;
    this.ended_at = data.ended_at;
    this.items = data.items;
    this.livemode = data.livemode;
    this.metadata = data.metadata;
    this.status = data.status;
  }
  setRaw(data) {
    this.raw = data;
    return this;
  }
  isCanceled() {
    return this.canceled_at !== null || this.canceled_at !== undefined;
  }
}
