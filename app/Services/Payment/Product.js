export class Product {
  constructor(data) {
    this.id = data.id;
    this.active = data.active;
    this.attributes = data.attributes;
    this.caption = data.caption;
    this.created = data.created;
    this.deactivate_on = data.deactivate_on;
    this.description = data.description;
    this.images = data.images;
    this.livemode = data.livemode;
    this.metadata = data.metadata;
    this.name = data.name;
    this.package_dimensions = data.package_dimensions;
    this.shippable = data.shippable;
    this.statement_descriptor = data.statement_descriptor;
    this.type = data.type;
    this.unit_label = data.unit_label;
    this.updated = data.updated;
    this.url = data.url;
  }
  setRaw(data) {
    this.raw = data;
    return this;
  }
}
