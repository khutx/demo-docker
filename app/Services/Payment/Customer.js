export class Customer {
  constructor(data) {
    this.id = data.id;
    this.account_balance = data.account_balance;
    this.created = data.created;
    this.currency = data.currency;
    this.description = data.description;
    this.discount = data.discount;
    this.email = data.email;
    this.invoice_prefix = data.invoice_prefix;
    this.livemode = data.livemode;
    this.shipping = data.shipping;
    this.subscriptions = data.subscriptions;
    this.sources = data.sources;
  }
  setRaw(data) {
    this.raw = data;
    return this;
  }
}
