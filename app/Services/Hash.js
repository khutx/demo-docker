const bcrypt = require('bcrypt-nodejs');
export class Hash {
  hash(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  }
  check(password, hash) {
    return bcrypt.compareSync(password, hash);
  }
}
