import UserRepository from '../Repositories/UserRepository';
import { Exception } from '@nsilly/exceptions';
import { App } from '@nsilly/container';
import { Auth } from '@nsilly/auth';
import _ from 'lodash';
module.exports = async function check(req, res, next) {
  const auth = Auth.id();
  const user = await App.make(UserRepository).findById(auth);
  const roleOfUser = await user.getRoles();
  for (let item = 0; item < roleOfUser.length; item++) {
    if (roleOfUser.length === 1) {
      const findRole = _.find(roleOfUser, { id: 3 });
      if (findRole) {
        throw new Exception(`Can't access !`, 1001);
      }
      next();
    } else {
      next();
    }
  }
};
