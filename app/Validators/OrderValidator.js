import { AbstractValidator, REQUIRED, IS_INT, IS_FLOAT } from './Validator';

export const RULE_SAVE_ORDER = 'create';
export const RULE_UPDATE_ORDER = 'udpate';
export const RULE_UPDATE_STATUS_ORDER = 'updateStatus';
export class OrderValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_SAVE_ORDER]: {
        user_id: [REQUIRED, IS_INT],
        payment_method: [REQUIRED, 'min:2', 'max:255'],
        amount: [REQUIRED, IS_FLOAT],
        note: [],
        shipping_method_id: [REQUIRED, IS_INT]
      },
      [RULE_UPDATE_ORDER]: {
        user_id: [IS_INT],
        payment_method: ['min:2', 'max:255'],
        amount: [IS_FLOAT],
        status: [IS_INT],
        note: [],
        shipping_method_id: [IS_INT]
      },
      [RULE_UPDATE_STATUS_ORDER] : {
        status : [IS_INT]
      }
    };
  }
}
