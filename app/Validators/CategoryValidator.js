import { AbstractValidator, REQUIRED } from './Validator';

export const RULE_SAVE_CATEGORY = 'create';
export class CategoryValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_SAVE_CATEGORY]: {
        name: [REQUIRED, 'min:2', 'max:50']
      }
    };
  }
}
