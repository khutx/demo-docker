import { AbstractValidator, REQUIRED, IS_INT } from './Validator';

export const RULE_CREATE = 'create';
export const RULE_UPDATE = 'udpate';

export class ImageValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_CREATE]: {
        imageable_type: [REQUIRED],
        imageable_id: [REQUIRED, IS_INT],
        url: [REQUIRED],
        type: [REQUIRED, 'min:2', 'max:255']
      },
      [RULE_UPDATE]: {
        imageable_type: [],
        imageable_id: [IS_INT],
        url: [],
        type: ['min:2', 'max:255']
      }
    };
  }
}
