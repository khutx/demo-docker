import { AbstractValidator, REQUIRED, IS_FLOAT } from './Validator';

export const RULE_CREATE = 'create';
export const RULE_UPDATE = 'udpate';

export class PlanValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_CREATE]: {
        interval: [REQUIRED, 'min:2', 'max:255'],
        nickname: [REQUIRED],
        currency: [REQUIRED],
        amount: [REQUIRED, IS_FLOAT],
        plan_id : [REQUIRED]
      },
      [RULE_UPDATE]: {
        interval: [REQUIRED, 'min:2', 'max:255'],
        nickname: [REQUIRED],
        currency: [REQUIRED],
        amount: [REQUIRED, IS_FLOAT],
        plan_id : [REQUIRED]
      }
    };
  }
}
