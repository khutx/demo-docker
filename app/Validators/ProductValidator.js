import { AbstractValidator, REQUIRED, IS_INT, IS_NUMBERIC } from './Validator';
export const RULE_SAVE_PRODUCT = 'create'
export const RULE_UPDATE_STATUS_OF_PRODUCT = 'updateStatus'
export class ProductValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_SAVE_PRODUCT]: {
        name : [REQUIRED],
        status : [IS_INT],
        weight : [IS_NUMBERIC]
      },
      [RULE_UPDATE_STATUS_OF_PRODUCT]: {
        starus :[IS_INT]
      }
    };
  }
}
