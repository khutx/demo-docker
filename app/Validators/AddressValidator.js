import { AbstractValidator, REQUIRED, IS_BOOLEAN } from './Validator';
export const RULE_SAVE_ADDRESS = 'save';
export class AddressValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_SAVE_ADDRESS]: {
        company: [REQUIRED],
        phone: [REQUIRED, 'min:10', 'max:11'],
        city: [REQUIRED],
        country: [REQUIRED],
        is_default: [IS_BOOLEAN],
        is_default_billing: [IS_BOOLEAN]
      }
    };
  }
}
