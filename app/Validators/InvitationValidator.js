import { AbstractValidator, IS_EMAIL } from './Validator';

export const RULE_CREATE = 'create';
export class InvitationValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_CREATE]: {
        email: [IS_EMAIL]
      }
    };
  }
}
