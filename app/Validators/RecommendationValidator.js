import { AbstractValidator, REQUIRED, IS_INT } from './Validator';

export const RULE_CREATE = 'create';
export const RULE_UPDATE = 'udpate';

export class RecommendationValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_CREATE]: {
        user_id: [REQUIRED, IS_INT],
        plan_id: [REQUIRED, IS_INT],
        quantity: [REQUIRED, IS_INT],
        note: [],
        status: [IS_INT]
      },
      [RULE_UPDATE]: {
        user_id: [IS_INT],
        plan_id: [IS_INT],
        quantity: [IS_INT],
        note: [],
        status: [IS_INT]
      }
    };
  }
}
