import { AbstractValidator, REQUIRED } from './Validator';
export const RULE_SAVE_FAQs = 'save';
export class FAQsValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_SAVE_FAQs]: {
        title: [REQUIRED, 'min:5', 'max: 100'],
        content: [REQUIRED, 'min:20']
      }
    };
  }
}
