import { AbstractValidator, REQUIRED } from './Validator';

export const RULE_CREATE = 'create';
export const RULE_UPDATE = 'update';

export class StatusValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_CREATE]: {
        name: [REQUIRED, 'min:2', 'max:10']
      },
      [RULE_UPDATE]: {
        name: [REQUIRED, 'min:2', 'max:10']
      }
    };
  }
}
