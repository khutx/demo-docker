import { AbstractValidator, REQUIRED, IS_EMAIL } from './Validator';

export const RULE_SAVE_EMAIL = 'save';
export class EmailValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_SAVE_EMAIL]: {
        email: [REQUIRED, IS_EMAIL]
      }
    };
  }
}
