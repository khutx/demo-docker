import { AbstractValidator, REQUIRED, IS_INT, IS_ISO_DATE } from './Validator';

export const RULE_CREATE = 'create';
export const RULE_UPDATE = 'update';

export class SubscriptionValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_CREATE]: {
        user_id: [REQUIRED, IS_INT],
        plan_id: [REQUIRED, IS_INT],
        start_time: [REQUIRED, IS_ISO_DATE],
        subscription_id: [REQUIRED, 'min:2', 'max:255']
      },
      [RULE_UPDATE]: {
        user_id: [IS_INT],
        plan_id: [IS_INT],
        start_time: [IS_ISO_DATE],
        subscription_id: ['min:2', 'max:255']
      }
    };
  }
}
