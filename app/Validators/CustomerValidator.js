import { AbstractValidator, REQUIRED } from './Validator';
export const RULE_SAVE = 'save';
export const RULE_UPDATE = 'update';
export class CustomerValidator extends AbstractValidator {
  static getRules() {
    return {
      [RULE_SAVE]: {
        first_name: [REQUIRED],
        last_name: [REQUIRED],
        gender: [REQUIRED],
        phone: [REQUIRED, 'min:10', 'max:11'],
        phone_contact: [REQUIRED],
        address: [REQUIRED],
        shipping_address: [REQUIRED],
        card_brand: [REQUIRED],
        card_last_four: [REQUIRED],
        card_expire: [REQUIRED],
        referal_code: [REQUIRED]
      },
      [RULE_UPDATE]: {
        first_name: [],
        last_name: [],
        gender: [],
        phone: ['min:10', 'max:11'],
        phone_contact: [],
        address: [],
        shipping_address: [],
        card_brand: [],
        card_last_four: [],
        card_expire: [],
        referal_code: []
      }
    };
  }
}
