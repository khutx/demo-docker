FROM khutran/node:10.15
WORKDIR /var/www/workspace
COPY . .
RUN yarn install
RUN yarn build
EXPOSE 3000
CMD ["node", "dist/bin/www"]
