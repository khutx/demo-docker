import express from 'express';
import ApiRouter from './api';
import { ApiResponse } from '@nsilly/response';

const router = express.Router();

router.use('/api', ApiRouter);

router.get('/', function(req, res) {
  res.json(ApiResponse.success());
});

export default router;
