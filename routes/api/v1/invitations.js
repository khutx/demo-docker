import express from 'express';
import InvitationRepository from '../../../app/Repositories/InvitationRepository';
import { SendThanksEmailNotification } from '../../../app/Notifications/SendThanksEmailNotification';
import { ApiResponse } from '@nsilly/response';
import { App } from '@nsilly/container';
import { AsyncMiddleware, Request } from '@nsilly/support';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.post('/', AsyncMiddleware(addListEmail));

async function addListEmail(req, res) {
  const data = Request.all().invitations;
  for (let item of data) {
    const email = await App.make(InvitationRepository).create(item);
    email.notify(new SendThanksEmailNotification());
  }
  res.json(ApiResponse.success());
}

export default router;
