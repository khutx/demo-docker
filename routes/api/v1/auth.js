import express from 'express';
import jwt from 'jsonwebtoken';
import { AsyncMiddleware, Request } from '@nsilly/support';
import { SendForgotPasswordNotification } from '../../../app/Notifications/SendForgotPasswordNotification';
// import { SendWelcomeEmailNotification } from '../../../app/Notifications/SendWelcomeEmailNotification';
import UserRepository from '../../../app/Repositories/UserRepository';
import { ApiResponse } from '@nsilly/response';
import RoleRepository from '../../../app/Repositories/RoleRepository';
import { Exception, NotFoundException, NotFoundHttpException } from '@nsilly/exceptions';
import { AuthValidator, REGISTER_RULE } from '../../../app/Validators/AuthValidator';
import { Hash } from '../../../app/Services/Hash';
import PasswordResetRepository from '../../../app/Repositories/PasswordResetRepository';
import { App } from '@nsilly/container';
import PasswordResets from '../../../app/Models/PasswordResets';
// import * as _ from 'lodash';
import UserFakeRepository from '../../../app/Repositories/UserFakeRepository';
import User from '../../../app/Models/User';

const router = express.Router();

router.get('/', (req, res) => {
  res.json(ApiResponse.success());
});
router.post('/login', AsyncMiddleware(login));
router.post('/register', AsyncMiddleware(register));
router.post('/forgot-password', AsyncMiddleware(forgotpassword));
router.post('/reset-password', AsyncMiddleware(resetpassword));
router.post('/fake-login', AsyncMiddleware(fake_login));
router.post('/anonymous', AsyncMiddleware(anonymous));

async function anonymous(req, res) {
  const password = App.make(Hash).hash(
    Math.random()
      .toString(36)
      .substring(2, 15)
  );
  const email = `${Math.random()
    .toString(36)
    .substring(2, 15)}@nutrio.com`;

  const user = await App.make(UserRepository).create({ email: email, password: password, status: 3 });
  const role = await App.make(RoleRepository).findById(3);

  const customer = {
    first_name: 'anonymous',
    last_name: 'anonymous'
  };
  const user_fake = await App.make(UserRepository).findById(user.id);
  await user_fake.createCustomer(customer);
  await user.addRole(role);

  const access_token = user_fake.generateToken();
  res.json({ access_token: access_token });
}

async function fake_login(req, res) {
  const data = Request.get('user_fake');
  const repository = new UserFakeRepository();
  const user_fake = await repository
    .where('user_fake', data)
    .with(User)
    .first();

  if (!user_fake) {
    throw new Exception('Login not illegal !', 1001);
  }

  if (!user_fake.user) {
    throw new Exception('User Fake not illegal !', 1001);
  }

  // if (user_fake.user.status !== 2) {
  //   throw new Exception('User Fake not illegal !', 1001);
  // }

  const access_token = user_fake.user.generateToken();
  await user_fake.destroy();
  res.json({ access_token: access_token });
}

async function login(req, res) {
  const email = Request.get('email');
  const password = Request.get('password');

  const repository = new UserRepository();
  const user = await repository.where('email', email).first();

  if (!user) {
    throw new NotFoundException('User');
  }

  const isValidPassword = App.make(Hash).check(password, user.password);

  if (isValidPassword === false) {
    throw new Exception('Password does not match', 4003);
  }

  await user.update({ last_login: new Date() });

  const access_token = user.generateToken();

  res.json({ access_token: access_token });
}

async function register(req, res) {
  AuthValidator.isValid(Request.all(), REGISTER_RULE);

  if (Request.get('password') !== Request.get('password_confirmation')) {
    throw new Error('Password does not match', 1000);
  }

  const data = {
    email: req.body.email,
    password: App.make(Hash).hash(Request.get('password'))
  };
  const repository = new UserRepository();
  const check = await repository.where('email', data.email).first();
  if (check) {
    throw new Exception('Email existing !', 1001);
  }

  const user = await repository.create(data);

  const role = await App.make(RoleRepository).findById(3);

  const customer = {
    first_name: 'first',
    last_name: 'last'
  };

  await user.createCustomer(customer);
  await user.addRole(role);

  // user.notify(new SendWelcomeEmailNotification());
  const access_token = user.generateToken();
  res.json({ access_token: access_token });
}

async function forgotpassword(req, res) {
  const email = req.body.email;

  const new_notify = new SendForgotPasswordNotification();

  const repository = new UserRepository();

  const user = await repository
    .where('email', email)
    .with(PasswordResets)
    .first();

  if (!user) {
    throw new NotFoundException('User');
  }

  const reset_token = user.generateToken();

  if (!user.password_reset) {
    await App.make(PasswordResetRepository).create({
      user_id: user.id,
      email: user.email,
      token: reset_token,
      status: 1
    });
  } else {
    await user.password_reset.update({
      user_id: user.id,
      email: user.email,
      token: reset_token,
      status: 1
    });
  }
  user.resetPasstoken(reset_token);
  user.notify(new_notify);

  res.json(ApiResponse.success());
}

async function resetpassword(req, res) {
  const reset_token = Request.get('token');
  const password = App.make(Hash).hash(Request.get('password'));

  const update_pass = {
    password: password
  };

  const verify = await jwt.verify(reset_token, process.env.JWT_SECRET);

  if (!verify) {
    throw new Exception('Token invalid', 4002);
  }

  const repository = new UserRepository();
  const user = await repository
    .where('email', verify.data.email)
    .with(PasswordResets)
    .first();

  if (!user.password_reset) {
    throw new NotFoundHttpException('User');
  } else if (user.password_reset.status !== 1) {
    throw new Exception('Token invalid', 4002);
  }

  await user.update(update_pass);
  await user.password_reset.update({ status: 0 });

  res.json(ApiResponse.success());
}

module.exports = router;
