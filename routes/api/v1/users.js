import express from 'express';
import UserTransformer from '../../../app/Transformers/UserTransformer';
import UserRepository from '../../../app/Repositories/UserRepository';
import Recommendation, { PENDING_UPDATE, NO_PENDING_UPDATE } from '../../../app/Models/Recommendation';
import { Request, AsyncMiddleware } from '@nsilly/support';
import { ApiResponse } from '@nsilly/response';
import { Exception } from '@nsilly/exceptions';
import { AuthMiddleware, Auth } from '@nsilly/auth';
import _ from 'lodash';
import Customer from '../../../app/Models/Customer';
import { CheckAuth } from '../../../app/Services/CheckAuth';
import AddressTransformer from '../../../app/Transformers/AddressTransformer';
import Address from '../../../app/Models/Address';
import { App } from '@nsilly/container';
import RecommendationTransformer from '../../../app/Transformers/RecommendationTransformer';
import RecommendationRepository from '../../../app/Repositories/RecommendationRepository';
import ProductRepository from '../../../app/Repositories/ProductRepository';
import Product from '../../../app/Models/Product';
import OrderTransformer from '../../../app/Transformers/OrderTransformer';
import { Hash } from '../../../app/Services/Hash';
import Subscription from '../../../app/Models/Subscription';
import PlanRepository from '../../../app/Repositories/PlanRepository';
import OrderItemRepository from '../../../app/Repositories/OrderItemRepository';
import { uncollectible, draft, open, paid } from '../../../app/Models/Order';
import Role from '../../../app/Models/Role';
import moment from 'moment';

const router = express.Router();

router.all('*', AuthMiddleware);

router.get('/:id/profile', AsyncMiddleware(profile));
router.get('/:id', AsyncMiddleware(show));
router.get('/:id/recommendation', AsyncMiddleware(getRecommendation));
router.get('/:id/orders', AsyncMiddleware(getOrder));
router.post('/:id/recommendation', AsyncMiddleware(addRecommendation));
router.put('/:id/recommendation', AsyncMiddleware(updateProductRecommendation));
router.put('/:id/recommendation/:recommendation_id', AsyncMiddleware(updateRecommendation));
router.get('/:id/products/:product_id', AsyncMiddleware(checkProductOfUser));
router.post('/:id/payment', AsyncMiddleware(AddCustomerStripe));
router.post('/:id/subscriptions', AsyncMiddleware(createSubscriptionStripe));
router.put('/:id/subscriptions/sync', AsyncMiddleware(syncSubscription));
router.put('/:id/subscriptions/:subscription_id', AsyncMiddleware(cancelSubscriptionStripe));
router.get('/:id/addresses', AsyncMiddleware(getAddressUser));
router.get('/:id/addresses/latest', AsyncMiddleware(getLastAddressUser));
router.post('/:id/addresses', AsyncMiddleware(addAddressUser));
router.get('/:id/payment/:subscription_id', AsyncMiddleware(getNextTimePayment));
router.delete('/:id/recommendation/:product_id', AsyncMiddleware(deleteProductRecomendation));
router.get('/:id/orders/latest', AsyncMiddleware(getLastOrderUser));
router.put('/:id/update', AsyncMiddleware(updateUser));
router.put('/change-password', AsyncMiddleware(changePassword));

async function checkProductOfUser(req, res) {
  const id = req.params.id;
  if (!App.make(CheckAuth).check(id)) {
    throw new Exception('Permission Denied', 1005);
  }
  const product_id = req.params.product_id;
  const product = await App.make(RecommendationRepository)
    .where('user_id', id)
    .where('product_id', product_id)
    .first();

  if (product) {
    res.json({ data: true });
  } else {
    res.json({ data: false });
  }
}

async function createSubscriptionStripe(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const repository = new UserRepository();
    const user = await repository
      .where('id', id)
      .with(Recommendation)
      .with(Customer)
      .with(Subscription)
      .first();
    // const email = user.email;
    const recommentdationOfUser = await user.getRecommendations();
    // const customer = await user.getCustomer();
    const checkRecommendation = _.isEmpty(recommentdationOfUser);
    if (!checkRecommendation) {
      const listAmount = [];
      for (let i = 0; i < recommentdationOfUser.length; i++) {
        const quantity = recommentdationOfUser[i].quantity;
        const product = await recommentdationOfUser[i].getProduct();
        const price = product.price;
        const amount = quantity * price;
        listAmount.push(amount);
        var total = _.sum(listAmount);
      }

      //   const planStripe = await App.make('PaymentGateway').createPlan({
      //     amount: total * 100,
      //     interval: 'month',
      //     product: {
      //       name: `Product for ${email}`
      //     },
      //     currency: process.env.CURRENCY,
      //     nickname: `Plan of ${email} `
      //   });

      //   const plan = await App.make(PlanRepository).create({
      //     nickname: planStripe.nickname,
      //     plan_id: planStripe.id,
      //     interval: planStripe.interval,
      //     amount: planStripe.amount,
      //     currency: planStripe.currency
      //   });
      //   const subscription = await App.make('PaymentGateway').subscribe({
      //     customer_id: customer.payment_id,
      //     plan: planStripe.id,
      //     status: 1
      //   });

      //   const invoice = await App.make('PaymentGateway').getInvoice(customer.payment_id);

      //   const order = {
      //     payment_method: invoice.billing,
      //     amount: invoice.total,
      //     status: invoice.status === 'draft' ? draft : invoice.status === 'paid' ? paid : invoice.status === 'uncollectible' ? uncollectible : invoice.status === 'open' ? open : 0,
      //     shipping_method_id: 1,
      //     next_payment_attempt: invoice.next_payment_attempt
      //   };

      const order = {
        payment_method: 'charge_automatically',
        amount: total * 100,
        status: 1,
        shipping_method_id: 1,
        next_payment_attempt: new Date(moment().add(30, 'day')).getTime() / 1000
      };
      const orderUser = await user.createOrder(order);

      for (const item of recommentdationOfUser) {
        await App.make(OrderItemRepository).create({ order_id: orderUser.id, product_id: item.product_id });
      }
      // const subscriptionOfUser = await user.getSubscription();
      // if (subscriptionOfUser) {
      //   await subscriptionOfUser.update({
      //     subscription_id: subscription.id,
      //     status: subscription.status,
      //     plan_id: plan.id
      //   });
      // } else {
      //   await user.createSubscription({
      //     subscription_id: subscription.id,
      //     status: subscription.status,
      //     plan_id: plan.id,
      //     start_time: new Date(subscription.created * 1000)
      //   });
      // }
      res.json(ApiResponse.success());
    } else {
      throw new Exception('Recommendation not exist product !', 1001);
    }
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function changePassword(req, res) {
  if (Request.get('password') !== Request.get('password_confirm')) {
    throw new Error('Password does not match', 1000);
  }

  const user = await Auth.user();
  await user.update({ password: App.make(Hash).hash(Request.get('password')), status: 1 });

  await user.reload();
  const access_token = user.generateToken();
  res.json({ access_token: access_token });
}

async function getOrder(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const repository = new UserRepository();
    const user = await repository
      .where('id', id)
      .withScope('getOrder')
      .first();

    res.json(ApiResponse.collection(user.orders, new OrderTransformer(['products'])));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function show(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const repository = new UserRepository();
    const user = await repository
      .where('id', id)
      .with(Customer)
      .first();

    res.json(ApiResponse.item(user, new UserTransformer(['customer'])));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function profile(req, res) {
  const user_id = req.params.id;
  if (App.make(CheckAuth).check(user_id)) {
    const repository = new UserRepository();
    let user = await repository
      .where('id', user_id)
      .with(Role)
      .first();

    if (!user) {
      throw new Exception('User not found', 1001);
    }

    res.json(ApiResponse.item(user, new UserTransformer(['roles'])));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function updateProductRecommendation(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const repository = new UserRepository();
    const products = Request.all().products;
    const user = await repository.findById(id);

    const recommendations = await user.getRecommendations();

    for (const item of products) {
      const product_id = item.id;
      delete item.id;
      const data = _.assign({ ...item }, { product_id: product_id });

      if (!_.find(recommendations, { product_id: product_id })) {
        await user.createRecommendation(data);
      }

      const listFind = _.remove(recommendations, n => {
        return n.product_id === product_id;
      });
      for (const find of listFind) {
        if (data.quantity !== 0) {
          await find.update(data);
        } else {
          await find.destroy();
        }
      }
    }

    for (const item of recommendations) {
      await item.destroy();
    }
    await user.reload();
    const result = await user.getRecommendations();
    res.json(ApiResponse.collection(result, new RecommendationTransformer()));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function addRecommendation(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const data = Request.all();
    const repository = new UserRepository();
    const user = await repository
      .where('id', id)
      .with(Recommendation)
      .first();

    if (!user) {
      throw new Exception(`User ${id} not found`, 1001);
    }

    const recommendation = _.find(user.recommendations, { product_id: parseInt(data.product_id) });
    if (!recommendation) {
      await App.make(ProductRepository).findById(data.product_id);
      await user.createRecommendation(data);
    } else {
      await recommendation.update({ quantity: recommendation.quantity + 1 });
    }

    await user.reload();
    res.json(ApiResponse.collection(user.recommendations, new RecommendationTransformer(['product'])));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function getRecommendation(req, res) {
  const id = req.params.id;

  // if (App.make(CheckAuth).check(id)) {
  const repository = new UserRepository();
  const user = await repository.findById(id);
  if (!user) {
    throw new Exception(`User ${id} not found`, 1001);
  }

  const recommendations = await App.make(RecommendationRepository)
    .where('user_id', id)
    // .withScope('getProduct')
    .with(Product)
    .get();

  for (const item of recommendations) {
    if (!_.isNil(item.product)) {
      item.product.images = await item.product.getImages();
      item.product.categories = await item.product.getCategories();
    }
  }

  res.json(ApiResponse.collection(recommendations, new RecommendationTransformer(['product'])));
  // } else {
  //   throw new Exception('Permission Denied', 1005);
  // }
}

async function updateRecommendation(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const recommendation_id = req.params.recommendation_id;
    const quantity = Request.get('quantity');
    const repository = new UserRepository();
    const user = await repository.findById(id);
    const recommendations = await user.getRecommendations();
    const recommendation = _.find(recommendations, { id: parseInt(recommendation_id) });

    if (!recommendation) {
      throw new Exception('User not exitis Recommendation !', 1001);
    }
    const quantityDB = recommendation.quantity;
    if (quantityDB !== quantity) {
      await recommendation.update({
        quantity: quantity,
        status: PENDING_UPDATE
      });
    }
    await user.reload();
    res.json(ApiResponse.success());
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function AddCustomerStripe(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const card_token = Request.get('card_token');
    const repository = new UserRepository();
    const user = await repository
      .where('id', id)
      .with(Customer)
      .first();
    const email = user.email;
    const customerOfUser = await user.getCustomer();
    const customer_stripe = await App.make('PaymentGateway').createCustomer({
      description: `Customer for ${email}`,
      source: card_token
    });
    const source = await App.make('PaymentGateway').getSourceCustomer(customer_stripe.id);
    await customerOfUser.update({
      payment_id: customer_stripe.id,
      card_last_four: source.last4,
      card_brand: source.brand,
      referal_code: source.address_zip,
      card_expire: `${source.exp_month}/${source.exp_year}`
    });
    res.json(ApiResponse.success());
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function cancelSubscriptionStripe(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const id_subscription = req.params.subscription_id;
    const repository = new UserRepository();
    const user = await repository.findById(id);
    const subscriptionOfUser = await user.getSubscription();
    const subscription = _.find(subscriptionOfUser, { id: parseInt(id_subscription) });
    const customer = await user.getCustomer();
    if (subscription.status === 'active') {
      const subCancel = await App.make('PaymentGateway').cancelSubscription(subscription.subscription_id);
      await subscriptionOfUser.update({ status: subCancel.status });
    } else {
      const planDB = await subscriptionOfUser.getPlan();
      const sub = await App.make('PaymentGateway').subscribe({
        customer_id: customer.payment_id,
        plan: planDB.plan_id
      });
      await subscriptionOfUser.update({
        subscription_id: sub.id,
        status: sub.status
      });
    }
    res.json(ApiResponse.success());
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function getAddressUser(req, res) {
  const user_id = req.params.id;
  if (App.make(CheckAuth).check(user_id)) {
    const repository = new UserRepository();
    const user = await repository.findById(user_id);
    const addresses = await user.getAddresses();
    res.json(ApiResponse.collection(addresses, new AddressTransformer()));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function getLastAddressUser(req, res) {
  const user_id = req.params.id;

  if (App.make(CheckAuth).check(user_id)) {
    const repository = new UserRepository();
    const user = await repository.findById(user_id);
    const addresses = await user.getAddresses();
    const address = _.last(addresses);
    res.json(ApiResponse.item(address, new AddressTransformer()));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function addAddressUser(req, res) {
  const user_id = req.params.id;
  if (App.make(CheckAuth).check(user_id)) {
    const data = Request.all();
    const repository = new UserRepository();
    const user = await repository
      .where('id', user_id)
      .with(Address)
      .first();
    await user.createAddress(data);
    res.json(ApiResponse.success());
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function getNextTimePayment(req, res) {
  const id = req.params.id;
  const id_subscription = req.params.subscription_id;
  const user = await App.make(UserRepository).findById(id);
  const subscriptionOfUser = await user.getSubscription();
  const subscription = _.find(subscriptionOfUser, { id: parseInt(id_subscription) });
  const subscriptionStripe = await App.make('PaymentGateway').getSubscriptionDetail(subscription.subscription_id);
  const nextTimePayment = new Date(subscriptionStripe.current_period_end * 1000);
  res.json(nextTimePayment);
}

async function deleteProductRecomendation(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const product_id = req.params.product_id;
    const user = await App.make(UserRepository).findById(id);
    const recommendations = await user.getRecommendations();

    const recommendation = _.find(recommendations, ['product_id', parseInt(product_id)]);
    if (recommendation) {
      await recommendation.destroy();
    }
    res.json(ApiResponse.success());
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function getLastOrderUser(req, res) {
  const user_id = req.params.id;

  if (App.make(CheckAuth).check(user_id)) {
    const repository = new UserRepository();
    const user = await repository.findById(user_id);
    const orders = await user.getOrders();
    const order = _.last(orders);
    res.json(ApiResponse.item(order, new OrderTransformer()));
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function syncSubscription(req, res) {
  const id = req.params.id;
  if (App.make(CheckAuth).check(id)) {
    const user = await App.make(UserRepository).findById(id);
    const email = user.email;
    const recommentdationOfUser = await user.getRecommendations();
    const subscriptionOfUser = await user.getSubscription();
    const listAmount = [];
    for (let i = 0; i < recommentdationOfUser.length; i++) {
      const quantity = recommentdationOfUser[i].quantity;
      const product = await recommentdationOfUser[i].getProduct();
      const price = product.price;
      const amount = quantity * price;
      listAmount.push(amount);
      var total = _.sum(listAmount);
    }

    // const planStripe = await App.make('PaymentGateway').createPlan({
    //   amount: total,
    //   interval: 'month',
    //   product: {
    //     name: `Product for ${email}`
    //   },
    //   currency: process.env.CURRENCY,
    //   nickname: `Plan of ${email} `
    // });
    // const plan = await subscriptionOfUser.getPlan();
    // await plan.update({ amount: parseFloat(total), plan_id: planStripe.id });
    // await App.make('PaymentGateway').updateSubscription(subscriptionOfUser.subscription_id, {
    //   plan: planStripe.id
    // });

    for (const item of recommentdationOfUser) {
      if (item.status === 'PENDING_UPDATE') {
        await item.update({ status: NO_PENDING_UPDATE });
      }
    }

    res.json(ApiResponse.success());
  } else {
    throw new Exception('Permission Denied', 1005);
  }
}

async function updateUser(req, res) {
  const id = req.params.id;
  if (!App.make(CheckAuth).check(id)) {
    throw new Exception('Permission Denied', 1005);
  }
  const data = Request.all();

  const user = await Auth.user();
  if (user.email !== data.email) {
    const findEmail = await App.make(UserRepository)
      .where('email', data.email)
      .first();

    if (findEmail) {
      throw new Exception('Email existed !', 1001);
    }

    const customerOfUser = await user.getCustomer();
    await user.update({ email: data.email });
    await customerOfUser.update({ first_name: data.first_name, last_name: data.last_name });
  } else {
    const customerOfUser = await user.getCustomer();
    await customerOfUser.update({ first_name: data.first_name, last_name: data.last_name });
  }

  res.json(ApiResponse.success());
}

export default router;
