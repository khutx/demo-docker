import express from 'express';
import { AsyncMiddleware } from '@nsilly/support';
import { AuthMiddleware } from '@nsilly/auth';
import { FileStorage } from '../../../app/Services/FileStorage';
import { ApiResponse } from '@nsilly/response';
import FileTransformer from '../../../app/Transformers/FileTransformer';
const router = express.Router();

router.all('*', AuthMiddleware);
router.post('/', AsyncMiddleware(upload));

async function upload(req, res) {
  const service = new FileStorage();

  const upload = await service
    .typeFile('image')
    .limitFileUpload(5)
    .uploadMultiFile('files');

  upload(req, res, function(error) {
    if (error) {
      res.status(500);
      res.json({ data: error.message, error_code: 1000 });
    } else {
      res.json(ApiResponse.collection(req.files, new FileTransformer()));
    }
  });
}

module.exports = router;
