import express from 'express';
import UserTransformer from '../../../app/Transformers/UserTransformer';
import UserRepository from '../../../app/Repositories/UserRepository';
import _ from 'lodash';
import Customer from '../../../app/Models/Customer';
import Role from '../../../app/Models/Role';
import { ApiResponse } from '@nsilly/response';
import { NotFoundException } from '@nsilly/exceptions';
import { AsyncMiddleware, Request } from '@nsilly/support';
import { AuthMiddleware, Auth } from '@nsilly/auth';

const router = express.Router();

router.all('*', AuthMiddleware);

router.get('/profile', AsyncMiddleware(profile));

/**
 * Show user profile
 *
 * @return ApiResponse.item
 */
async function profile(req, res) {
  const user = await Auth.user();
  const repository = new UserRepository();
  const transformer = new UserTransformer();
  let query = repository.where('id', user.id);

  const isCustomer = await user.isRole('user');

  if (isCustomer) {
    query = repository.with(Customer);
    transformer.with('customer');
  }

  if (Request.has('includes')) {
    const includes = _.trim(Request.get('includes')).split(',');
    if (_.includes(includes, 'roles')) {
      query = repository.with(Role);
      transformer.with('roles');
    }
  }

  const result = await query.first();

  if (!result) {
    throw new NotFoundException('User');
  }

  res.json(ApiResponse.item(result, transformer));
}

module.exports = router;
