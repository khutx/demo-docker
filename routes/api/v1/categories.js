import express from 'express';
import CategoryRepository from '../../../app/Repositories/CategoryRepository';
import CategoryTransformer from '../../../app/Transformers/CategoryTransformer';
import { ApiResponse } from '@nsilly/response';
import { AsyncMiddleware } from '@nsilly/support';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.post('/list', AsyncMiddleware(list));

async function index(req, res) {
  const repository = new CategoryRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();
  const categories = await repository.paginate();
  res.json(ApiResponse.paginate(categories, new CategoryTransformer()));
}

async function list(req, res) {
  const categories = await new CategoryRepository().get();
  res.json(ApiResponse.collection(categories, new CategoryTransformer()));
}

export default router;
