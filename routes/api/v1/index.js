import express from 'express';
import admin from './admin';
import auth from './auth';
import me from './me';
import upload from './upload';
import user from './users';
import product from './products';
import category from './categories';
import faq from './faqs';
import webhoock from './webhoock';
import email from './emails';
import fakeUser from './fakeUser';
import invitation from './invitations';

const router = express.Router();

router.use('/admin', admin);
router.use('/auth', auth);
router.use('/me', me);
router.use('/upload', upload);
router.use('/users', user);
router.use('/products', product);
router.use('/categories', category);
router.use('/faqs', faq);
router.use('/webhoock', webhoock);
router.use('/emails', email);
router.use('/user_fake', fakeUser);
router.use('/invitations', invitation);

export default router;
