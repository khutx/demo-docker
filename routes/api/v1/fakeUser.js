import express from 'express';
import { Request, AsyncMiddleware } from '@nsilly/support';
import { ApiResponse } from '@nsilly/response';
import _ from 'lodash';
import { App } from '@nsilly/container';
import RecommendationTransformer from '../../../app/Transformers/RecommendationTransformer';
import Product from '../../../app/Models/Product';
import FakeRecommendationRepository from '../../../app/Repositories/FakeRecommendationRepository';

const router = express.Router();

router.get('/:user_fake/fake_recommendation', AsyncMiddleware(getFakeRecommendation));
router.put('/:user_fake/fake_recommendation', AsyncMiddleware(updateProductFakeRecommendation));
router.delete('/:user_fake/fake_recommendation/:product_id', AsyncMiddleware(deleteProductFakeRecomendation));

async function deleteProductFakeRecomendation(req, res) {
  const product_id = req.params.product_id;
  const user_fake = req.params.user_fake;
  const recommendations = await App.make(FakeRecommendationRepository)
    .where('user_id', user_fake)
    .where('product_id', product_id)
    .first();
  await recommendations.destroy();
  res.json(ApiResponse.success());
}

async function updateProductFakeRecommendation(req, res) {
  const user_fake = req.params.user_fake;
  const products = Request.all().products;

  const repository = new FakeRecommendationRepository();
  const recommendations = await repository
    .where('user_id', user_fake)
    .with(Product)
    .get();

  for (const item of products) {
    const product_id = item.id;
    delete item.id;
    const data = _.assign({ ...item }, { product_id: product_id });

    if (!_.find(recommendations, { product_id: product_id })) {
      await App.make(FakeRecommendationRepository).create(_.assign(data, { user_id: user_fake }));
    }

    const listFind = _.remove(recommendations, n => {
      return n.product_id === product_id;
    });
    for (const find of listFind) {
      await find.update(data);
    }
  }

  for (const item of recommendations) {
    await item.destroy();
  }

  const result = await App.make(FakeRecommendationRepository)
    .where('user_id', user_fake)
    .with(Product)
    .get();

  res.json(ApiResponse.collection(result, new RecommendationTransformer()));
}

async function getFakeRecommendation(req, res) {
  const id = req.params.user_fake;

  const recommendations = await App.make(FakeRecommendationRepository)
    .where('user_id', id)
    .with(Product)
    .get();

  for (const item of recommendations) {
    if (!_.isNil(item.product)) {
      item.product.images = await item.product.getImages();
    }
  }

  res.json(ApiResponse.collection(recommendations, new RecommendationTransformer(['product'])));
}

export default router;
