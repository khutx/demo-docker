import express from 'express';
import ProductRepository from '../../../app/Repositories/ProductRepository';
import Image from '../../../app/Models/Image';
import Category from '../../../app/Models/Category';
import ProductTransformer from '../../../app/Transformers/ProductTransformer';
import { App } from '@nsilly/container';
import { ApiResponse } from '@nsilly/response';
import { AsyncMiddleware, Request } from '@nsilly/support';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';
import _ from 'lodash';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/list', AsyncMiddleware(list));

async function list(req, res) {
  const category = Request.get('category');

  if (category) {
    const products = await App.make(ProductRepository)
      .applyOrderFromRequest()
      .whereHas(Category, q => {
        q.where('slug', category);
        return q;
      })
      .get();

    const result = await App.make(ProductRepository)
      .whereIn('id', _.map(products, item => item.id))
      .with(Category)
      .with(Image)
      .get();

    res.json(ApiResponse.collection(result, new ProductTransformer(['images', 'categories'])));
  } else {
    const result = await App.make(ProductRepository)
      .applyOrderFromRequest()
      .with(Image)
      .with(Category)
      .get();

    res.json(ApiResponse.collection(result, new ProductTransformer(['images', 'categories'])));
  }
}

async function index(req, res) {
  const repository = new ProductRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest();
  repository.applyOrderFromRequest();
  const result = await repository
    .with(Image)
    .with(Category)
    .paginate();

  res.json(ApiResponse.paginate(result, new ProductTransformer(['images', 'categories'])));
}

async function show(req, res) {
  const product_id = req.params.id;
  const repository = new ProductRepository();

  const product = await repository
    .where('id', product_id)
    .with(Image)
    .with(Category)
    .first();

  res.json(ApiResponse.item(product, new ProductTransformer(['images', 'categories'])));
}

export default router;
