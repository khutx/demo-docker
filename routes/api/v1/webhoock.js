import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
// import { AuthMiddleware } from '@nsilly/auth';
import { ApiResponse } from '@nsilly/response';
import * as _ from 'lodash';
import { App } from '@nsilly/container';
import ProductRepository from '../../../app/Repositories/ProductRepository';
import { renderQuestions } from '../../../app/Util/renderQuestions';
import { Hash } from '../../../app/Services/Hash';
import UserRepository from '../../../app/Repositories/UserRepository';
import UserFakeRepository from '../../../app/Repositories/UserFakeRepository';
import RecommendationRepository from '../../../app/Repositories/RecommendationRepository';
import RoleRepository from '../../../app/Repositories/RoleRepository';
import CustomerRepository from '../../../app/Repositories/CustomerRepository';
import OrderItemRepository from '../../../app/Repositories/OrderItemRepository';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.post('/typeform', AsyncMiddleware(typefrom));
router.post('/stripe', AsyncMiddleware(stripe));

async function stripe(req, res) {
  const data = Request.get('data').object;

  console.log(data);
  const customer = await App.make(CustomerRepository)
    .where('payment_id', data.customer)
    .first();

  const user = await App.make(UserRepository).findById(customer.user_id);
  const recommendations = await user.getRecommendations();
  const order = {
    payment_method: 5,
    amount: data.total,
    status: data.status === 'draft' ? 2 : 1,
    shipping_method_id: 1,
    next_payment_attempt: data.next_payment_attempt
  };

  const orderUser = await user.createOrder(order);

  for (const item of recommendations) {
    await App.make(OrderItemRepository).create({ order_id: orderUser.id, product_id: item.product_id });
  }

  res.json(ApiResponse.success());
}

async function typefrom(req, res) {
  const data = Request.get('form_response');
  const id = data['hidden']['id'];

  // if (data['form_id'] !== process.env.FORM_ID) {
  //   throw new Exception('Request illegal', 1001);
  // }

  const answers = data['answers'];
  const questions = data['definition']['fields'];

  const result = renderQuestions(questions, answers);

  // console.log(result);
  const customer = result['customer'];
  const password = App.make(Hash).hash(
    Math.random()
      .toString(36)
      .substring(2, 15)
  );

  // const checkId = await App.make(UserRepository)
  //   .where('id', id)
  //   .first();

  // const check = !_.isNil(checkId)
  //   ? checkId
  //   : await App.make(UserRepository)
  //       .where('email', customer.email)
  //       .first();

  // console.log(result);
  const check = await App.make(UserRepository)
    .where('email', customer.email)
    .first();

  if (!check) {
    const user = await App.make(UserRepository).create({ email: customer.email, password: password, status: 2 });
    const role = await App.make(RoleRepository).findById(3);

    const newCustomer = {
      first_name: customer.first_name,
      last_name: customer.first_name
    };

    const user_fake = await App.make(UserRepository).findById(user.id);
    await user_fake.createCustomer(newCustomer);
    await user.addRole(role);
    await App.make(UserFakeRepository).create({ user_id: user.id, user_fake: id });
    for (const item of result['recommendations']) {
      if (!_.isNull(item.sku)) {
        const product = await App.make(ProductRepository)
          .where('sku', item.sku)
          .first();

        if (product) {
          const recommendation = await App.make(RecommendationRepository)
            .where('product_id', product.id)
            .where('user_id', user.id)
            .first();

          if (recommendation) {
            await recommendation.update({
              quantity: recommendation.quantity + item.quantity,
              note: !_.isEmpty(item.note) ? `${recommendation.note}, ${item.note}` : recommendation.note
            });
          } else {
            await user_fake.createRecommendation({ product_id: product.id, note: item.note, quantity: item.quantity });
          }
        }
      }
    }
  } else {
    await App.make(UserFakeRepository).create({ user_id: check.id, user_fake: id });
    const customerUser = await check.getCustomer();
    if (customerUser) {
      await customerUser.update({
        first_name: customer.first_name,
        last_name: customer.first_name
      });
    }

    const recommendations = await check.getRecommendations();
    for (const item of recommendations) {
      await item.destroy();
    }

    for (const item of result['recommendations']) {
      if (!_.isNull(item.sku)) {
        const product = await App.make(ProductRepository)
          .where('sku', item.sku)
          .first();

        if (product) {
          const recommendation = await App.make(RecommendationRepository)
            .where('product_id', product.id)
            .where('user_id', check.id)
            .first();

          if (recommendation) {
            await recommendation.update({
              quantity: recommendation.quantity + item.quantity,
              note: !_.isEmpty(item.note) ? `${recommendation.note}, ${item.note}` : recommendation.note
            });
          } else {
            await check.createRecommendation({ product_id: product.id, note: item.note, quantity: item.quantity });
          }
        }
      }
    }
  }

  res.json(ApiResponse.success());
}

module.exports = router;
