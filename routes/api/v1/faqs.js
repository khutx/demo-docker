import express from 'express';
import FAQsRepository from '../../../app/Repositories/FAQsRepository';
import FAQsTransformer from '../../../app/Transformers/FAQsTransformer';
import { ApiResponse } from '@nsilly/response';
import { AsyncMiddleware } from '@nsilly/support';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.post('/', AsyncMiddleware(list));

async function index(req, res) {
  const repository = new FAQsRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();
  const faq = await repository.paginate();
  res.json(ApiResponse.paginate(faq, new FAQsTransformer()));
}

async function list(req, res) {
  const faq = await new FAQsRepository().get();
  res.json(ApiResponse.collection(faq, new FAQsTransformer()));
}

export default router;
