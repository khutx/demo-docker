import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
import { ApiResponse } from '@nsilly/response';
// import * as _ from 'lodash';
import PlanRepository from '../../../../app/Repositories/PlanRepository';
import PlanTransformer from '../../../../app/Transformers/PlanTransformer';
import { PlanValidator, RULE_UPDATE, RULE_CREATE } from '../../../../app/Validators/PlanValidator';
import { App } from '@nsilly/container';
import { AuthMiddleware } from '@nsilly/auth';

const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new PlanRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();

  const result = await repository.paginate();
  res.json(ApiResponse.paginate(result, new PlanTransformer()));
}

async function show(req, res) {
  const id = req.params.id;
  const repository = new PlanRepository();
  const result = await repository.findById(id);
  res.json(ApiResponse.item(result, new PlanTransformer()));
}

async function create(req, res) {
  PlanValidator.isValid(Request.all(), RULE_CREATE);
  const data = Request.all();
  const repository = new PlanRepository();
  const result = await repository.create(data);
  res.json(ApiResponse.item(result, new PlanTransformer()));
}

async function update(req, res) {
  PlanValidator.isValid(Request.all(), RULE_UPDATE);
  const repository = new PlanRepository();
  const result = await repository.update(Request.all(), req.params.id);
  res.json(ApiResponse.item(result, new PlanTransformer()));
}

async function destroy(req, res) {
  App.make(PlanRepository).deleteById(req.params.id);
  res.json(ApiResponse.success());
}

export default router;
