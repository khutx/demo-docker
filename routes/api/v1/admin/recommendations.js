import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
import { ApiResponse } from '@nsilly/response';
// import * as _ from 'lodash';
import RecommendationRepository from '../../../../app/Repositories/RecommendationRepository';
import RecommendationTransformer from '../../../../app/Transformers/RecommendationTransformer';
import { RecommendationValidator, RULE_CREATE, RULE_UPDATE } from '../../../../app/Validators/RecommendationValidator';
import Product from '../../../../app/Models/Product';
import { App } from '@nsilly/container';
import { AuthMiddleware } from '@nsilly/auth';

const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new RecommendationRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();

  const result = await repository.paginate();
  res.json(ApiResponse.paginate(result, new RecommendationTransformer()));
}

async function show(req, res) {
  const id = req.params.id;
  const repository = new RecommendationRepository();
  const result = await repository
    .where('id', id)
    .with(Product)
    .first();
  res.json(ApiResponse.item(result, new RecommendationTransformer(['product'])));
}

async function create(req, res) {
  RecommendationValidator.isValid(Request.all(), RULE_CREATE);
  const data = Request.all();
  const repository = new RecommendationRepository();
  const result = await repository.create(data);
  res.json(ApiResponse.item(result, new RecommendationTransformer()));
}

async function update(req, res) {
  RecommendationValidator.isValid(Request.all(), RULE_UPDATE);
  const repository = new RecommendationRepository();
  const result = await repository.update(Request.all(), req.params.id);
  res.json(ApiResponse.item(result, new RecommendationTransformer()));
}

async function destroy(req, res) {
  App.make(RecommendationRepository).deleteById(req.params.id);
  res.json(ApiResponse.success());
}

export default router;
