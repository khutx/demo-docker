import express from 'express';
import CustomerRepository from '../../../../app/Repositories/CustomerRepository';
import CustomerTransformer from '../../../../app/Transformers/CustomerTransformer';
import { CustomerValidator, RULE_SAVE } from '../../../../app/Validators/CustomerValidator';
import User from '../../../../app/Models/User';
import { App } from '@nsilly/container';
import { Request, AsyncMiddleware } from '@nsilly/support';
import { ApiResponse } from '@nsilly/response';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new CustomerRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();

  const result = await repository.with(User).paginate();

  res.json(ApiResponse.paginate(result, new CustomerTransformer(['user'])));
}

async function show(req, res) {
  const id = req.params.id;
  const repository = new CustomerRepository();
  const result = await repository
    .where('id', id)
    .with(User)
    .first();
  res.json(ApiResponse.item(result, new CustomerTransformer(['user'])));
}

async function create(req, res) {
  CustomerValidator.isValid(Request.all(), RULE_SAVE);
  const data = Request.all();
  const repository = new CustomerRepository();
  const result = await repository.create(data);
  res.json(ApiResponse.item(result, new CustomerTransformer()));
}

async function update(req, res) {
  CustomerValidator.isValid(Request.all(), RULE_SAVE);
  const customer_id = req.params.id;
  const address = await App.make(CustomerRepository).findById(customer_id);
  const result = await address.update(Request.all());
  res.json(ApiResponse.item(result, new CustomerTransformer()));
}

async function destroy(req, res) {
  const customer_id = req.params.id;
  const customer = await App.make(CustomerRepository).findById(customer_id);
  await customer.destroy(customer_id);
  res.json(ApiResponse.success());
}

export default router;
