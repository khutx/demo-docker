import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
import StatusRepository from '../../../../app/Repositories/StatusRepository';
import StatusTransformer from '../../../../app/Transformers/StatusTransformer';
import { ApiResponse } from '@nsilly/response';
import _ from 'lodash';
import { StatusValidator, RULE_CREATE, RULE_UPDATE } from '../../../../app/Validators/StatusValidator';
import { AuthMiddleware } from '@nsilly/auth';
import { App } from '@nsilly/container';
const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.post('/list', AsyncMiddleware(list));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function list(req, res) {
  const repository = new StatusRepository();

  const result = await repository.get();

  res.json(ApiResponse.collection(result, new StatusTransformer()));
}

async function index(req, res) {
  const repository = new StatusRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();

  const result = await repository.paginate();

  res.json(ApiResponse.paginate(result, new StatusTransformer()));
}

async function show(req, res) {
  const id = req.params.id;
  const repository = new StatusRepository();
  const result = await repository.findById(id);
  res.json(ApiResponse.item(result, new StatusTransformer()));
}

async function create(req, res) {
  StatusValidator.isValid(Request.all(), RULE_CREATE);
  const data = Request.all();
  data.slug = _.snakeCase(Request.get('name'));
  const repository = new StatusRepository();
  const result = await repository.create(data);
  res.json(ApiResponse.item(result, new StatusTransformer()));
}

async function update(req, res) {
  StatusValidator.isValid(Request.all(), RULE_UPDATE);
  const repository = new StatusRepository();
  const result = await repository.update(Request.all(), req.params.id);
  res.json(ApiResponse.item(result, new StatusTransformer()));
}

async function destroy(req, res) {
  App.make(StatusRepository).deleteById(req.params.id);
  res.json(ApiResponse.success());
}

export default router;
