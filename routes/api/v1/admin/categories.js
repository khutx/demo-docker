import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
import { App } from '@nsilly/container';
import { ApiResponse } from '@nsilly/response';
import CategoryRepository from '../../../../app/Repositories/CategoryRepository';
import CategoryTransformer from '../../../../app/Transformers/CategoryTransformer';
import { CategoryValidator, RULE_SAVE_CATEGORY } from '../../../../app/Validators/CategoryValidator';
import _ from 'lodash';
import ProductTransformer from '../../../../app/Transformers/ProductTransformer';
import Image from '../../../../app/Models/Image';
import { AuthMiddleware } from '@nsilly/auth';

const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id/products', AsyncMiddleware(getProdcutsCategory));
router.post('/list', AsyncMiddleware(list));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new CategoryRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();
  const result = await repository.paginate();
  res.json(ApiResponse.paginate(result, new CategoryTransformer()));
}

async function list(req, res) {
  const result = await new CategoryRepository().get();
  res.json(ApiResponse.collection(result, new CategoryTransformer()));
}

async function show(req, res) {
  const id = req.params.id;
  const repository = new CategoryRepository();
  const result = await repository.findById(id);
  res.json(ApiResponse.item(result, new CategoryTransformer()));
}

async function create(req, res) {
  CategoryValidator.isValid(Request.all(), RULE_SAVE_CATEGORY);
  const category = Request.all();
  category.slug = _.snakeCase(Request.get('name'));
  const repository = new CategoryRepository();
  const result = await repository.create(category);
  res.json(ApiResponse.item(result, new CategoryTransformer()));
}

async function update(req, res) {
  CategoryValidator.isValid(Request.all(), RULE_SAVE_CATEGORY);
  const category = Request.all();
  category.slug = _.snakeCase(Request.get('name'));
  const repository = new CategoryRepository();
  const result = await repository.update(category, req.params.id);
  res.json(ApiResponse.item(result, new CategoryTransformer()));
}

async function destroy(req, res) {
  App.make(CategoryRepository).deleteById(req.params.id);
  res.json(ApiResponse.success());
}

async function getProdcutsCategory(req, res) {
  const id = req.params.id;
  const repository = new CategoryRepository();
  const category = await repository.findById(id);
  const products = await category.getProducts({ include: [{ model: Image, scope: { imageable_type: 'product' } }] });
  res.json(ApiResponse.collection(products, new ProductTransformer(['images'])));
}

export default router;
