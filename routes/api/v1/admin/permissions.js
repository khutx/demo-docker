import express from 'express';
import { AsyncMiddleware } from '@nsilly/support';
import { AuthMiddleware } from '@nsilly/auth';
import { ApiResponse } from '@nsilly/response';
import PermissionGroupRepository from '../../../../app/Repositories/PermissionGroupRepository';
import PermissionGroupTransformer from '../../../../app/Transformers/PermissionGroupTransformer';
import { App } from '@nsilly/container';
import Permission from '../../../../app/Models/Permission';

const router = express.Router();

router.all('*', AuthMiddleware);

router.get('/group', AsyncMiddleware(group));

async function group(req, res) {
  const result = await App.make(PermissionGroupRepository)
    .with(Permission)
    .get();
  res.json(ApiResponse.collection(result, new PermissionGroupTransformer(['permissions'])));
}

module.exports = router;
