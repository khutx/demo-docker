import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';
import FAQsRepository from '../../../../app/Repositories/FAQsRepository';
import FAQsTransformer from '../../../../app/Transformers/FAQsTransformer';
import { FAQsValidator, RULE_SAVE_FAQs } from '../../../../app/Validators/FAQsValidator';
import { AuthMiddleware } from '@nsilly/auth';
import { App } from '@nsilly/container';
import { ApiResponse } from '@nsilly/response';

const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.post('/list', AsyncMiddleware(list));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new FAQsRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();
  const faq = await repository.paginate();
  res.json(ApiResponse.paginate(faq, new FAQsTransformer()));
}

async function list(req, res) {
  const faq = await App.make(FAQsRepository).get();
  res.json(ApiResponse.collection(faq, new FAQsTransformer()));
}

async function create(req, res) {
  // FAQsValidator.isValid(Request.all(), RULE_SAVE_FAQs);
  const faq = await App.make(FAQsRepository).create(Request.all());
  res.json(ApiResponse.item(faq, new FAQsTransformer()));
}

async function update(req, res) {
  // FAQsValidator.isValid(Request.all(), RULE_SAVE_FAQs);
  const id = req.params.id;
  const faq = await App.make(FAQsRepository).findById(id);
  await faq.update(Request.all());
  res.json(ApiResponse.success());
}

async function destroy(req, res) {
  const id = req.params.id;
  const faq = await App.make(FAQsRepository).findById(id);
  await faq.destroy(id);
  res.json(ApiResponse.success());
}

export default router;
