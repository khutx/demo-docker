import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
import { ApiResponse } from '@nsilly/response';
// import * as _ from 'lodash';
import { SubscriptionValidator, RULE_CREATE, RULE_UPDATE } from '../../../../app/Validators/SubscriptionValidator';
import SubscriptionRepository from '../../../../app/Repositories/SubscriptionRepository';
import SubscriptionTransformer from '../../../../app/Transformers/SubscriptionTransformer';
import User from '../../../../app/Models/User';
import Plan from '../../../../app/Models/Plan';
import { App } from '@nsilly/container';
import { AuthMiddleware } from '@nsilly/auth';
const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new SubscriptionRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();

  const result = await repository
    .with(User)
    .with(Plan)
    .paginate();

  res.json(ApiResponse.paginate(result, new SubscriptionTransformer(['user', 'plan'])));
}

async function show(req, res) {
  const id = req.params.id;
  const repository = new SubscriptionRepository();
  const result = await repository
    .where('id', id)
    .with(User)
    .with(Plan)
    .first();

  res.json(ApiResponse.item(result, new SubscriptionTransformer(['user', 'plan'])));
}

async function create(req, res) {
  SubscriptionValidator.isValid(Request.all(), RULE_CREATE);
  const data = Request.all();
  const repository = new SubscriptionRepository();
  const result = await repository.create(data);
  res.json(ApiResponse.item(result, new SubscriptionTransformer()));
}

async function update(req, res) {
  SubscriptionValidator.isValid(Request.all(), RULE_UPDATE);
  const repository = new SubscriptionRepository();
  const result = await repository.update(Request.all(), req.params.id);
  res.json(ApiResponse.item(result, new SubscriptionTransformer()));
}

async function destroy(req, res) {
  App.make(SubscriptionRepository).deleteById(req.params.id);
  res.json(ApiResponse.success());
}

export default router;
