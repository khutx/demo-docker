import express from 'express';
import EmailRepository from '../../../../app/Repositories/EmailRepository';
import { ExprotCSV } from '../../../../app/Services/ExportCSV';
import { ApiResponse } from '@nsilly/response';
import { Exception } from '@nsilly/exceptions';
import { AuthMiddleware } from '@nsilly/auth';
import { AsyncMiddleware, Request } from '@nsilly/support';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';

const router = express.Router();

router.all('*', AuthMiddleware);
router.put('/:id/status', AsyncMiddleware(changeStatus));
router.post('/export', AsyncMiddleware(exportFileCSV));

async function changeStatus(req, res) {
  const id = req.params.id;
  const repository = new EmailRepository();
  const email = await repository.findById(id);
  if (!email) {
    throw new Exception('Project not found', 1001);
  }

  const status = Request.get('status');
  await email.update({ status: status });
  res.json(ApiResponse.success());
}

async function exportFileCSV(req, res) {
  const model = 'emails';
  const repository = new EmailRepository();
  const emails = await repository.get();
  var fields = ['email', 'first_name', 'last_name', 'status'];
  const data = await new ExprotCSV().pushS3Bucket(emails, fields, model);
  res.json(data);
}
export default router;
