import express from 'express';
import AddressRepository from '../../../../app/Repositories/AddressRepository';
import { ApiResponse } from '@nsilly/response';
import { App } from '@nsilly/container';
import { AsyncMiddleware, Request } from '@nsilly/support';
import AddressTransformer from '../../../../app/Transformers/AddressTransformer';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';
import { AddressValidator, RULE_SAVE_ADDRESS } from '../../../../app/Validators/AddressValidator';
import { AuthMiddleware } from '@nsilly/auth';

const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new AddressRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();
  const address = await repository.paginate();
  res.json(ApiResponse.paginate(address, new AddressTransformer()));
}

async function show(req, res) {
  const address = await App.make(AddressRepository).findById(req.params.id);
  res.json(ApiResponse.item(address, new AddressTransformer()));
}

async function create(req, res) {
  AddressValidator.isValid(Request.all(), RULE_SAVE_ADDRESS);
  const address = await App.make(AddressRepository).create(Request.all());
  res.json(ApiResponse.item(address, new AddressTransformer()));
}

async function update(req, res) {
  AddressValidator.isValid(Request.all(), RULE_SAVE_ADDRESS);
  const address_id = req.params.id;
  const address = await App.make(AddressRepository).findById(address_id);
  await address.update(Request.all());
  res.json(ApiResponse.success());
}

async function destroy(req, res) {
  const address_id = req.params.id;
  const address = await App.make(AddressRepository).findById(address_id);
  await address.destroy(address_id);
  res.json(ApiResponse.success());
}

export default router;
