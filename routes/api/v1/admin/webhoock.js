import express from 'express';
import UserRepository from '../../../../app/Repositories/UserRepository';
import { AsyncMiddleware, Request } from '@nsilly/support';
// import { AuthMiddleware } from '@nsilly/auth';
import { App } from '@nsilly/container';
import { ApiResponse } from '@nsilly/response';
// import { Exception } from '@nsilly/exceptions/dist/src/Exceptions/Exception';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.post('/', AsyncMiddleware(webhoock));

async function webhoock(req, res) {
  const data = Request.get('form_response');
  const id = data['hidden']['id'];
  // if (data['form_id'] !== process.env.FORM_ID) {
  //   throw new Exception('Request illegal', 1001);
  // }

  const user = await App.make(UserRepository).findById(id);
  const recommendationUser = await user.getRecommendations();

  for (const item of recommendationUser) {
    await item.destroy();
  }
  // const recommendations = await function(data['answers']);
  const recommendations = [{ product_id: 1, quantity: 10 }, { product_id: 2, quantity: 20 }];

  for (const item of recommendations) {
    await user.createRecommendation(item);
  }
  res.json(ApiResponse.success());
}

module.exports = router;
