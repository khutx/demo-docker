import express from 'express';
import users from './users';
import status from './status';
import categories from './categories';
import products from './products';
import orders from './orders';
import plans from './plans';
import recommendations from './recommendations';
import subscriptions from './subscriptions';
import images from './images';
import roles from './roles';
import permissions from './permissions';
import addresses from './addresses';
import customer from './customers';
import faq from './faqs';
import webhoock from './webhoock';
import email from './emails';

const router = express.Router();

router.use('/users', users);
router.use('/status', status);
router.use('/categories', categories);
router.use('/products', products);
router.use('/orders', orders);
router.use('/plans', plans);
router.use('/recommendations', recommendations);
router.use('/subscriptions', subscriptions);
router.use('/images', images);
router.use('/roles', roles);
router.use('/permissions', permissions);
router.use('/addresses', addresses);
router.use('/customers', customer);
router.use('/faqs', faq);
router.use('/webhoock', webhoock);
router.use('/emails', email);

export default router;
