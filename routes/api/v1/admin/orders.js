import express from 'express';
import { AsyncMiddleware, Request } from '@nsilly/support';
import { ApiResponse } from '@nsilly/response';
// import * as _ from 'lodash';
import { OrderValidator, RULE_SAVE_ORDER, RULE_UPDATE_ORDER, RULE_UPDATE_STATUS_ORDER } from '../../../../app/Validators/OrderValidator';
import OrderRepository from '../../../../app/Repositories/OrderRepository';
import OrderTransformer from '../../../../app/Transformers/OrderTransformer';
import Product from '../../../../app/Models/Product';
import User from '../../../../app/Models/User';
import { Exception } from '@nsilly/exceptions';
import { App } from '@nsilly/container';
import { AuthMiddleware } from '@nsilly/auth';

const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));
router.put('/:id/status', AsyncMiddleware(updateStatus));

async function index(req, res) {
  const repository = new OrderRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();

  const result = await repository
    .with(User)
    .with(Product)
    .paginate();
  res.json(ApiResponse.paginate(result, new OrderTransformer(['user', 'products'])));
}

async function show(req, res) {
  const id = req.params.id;
  const repository = new OrderRepository();

  const order = await repository
    .where('id', id)
    .with(User)
    .with(Product)
    .first();

  res.json(ApiResponse.item(order, new OrderTransformer(['user', 'products'])));
}

async function create(req, res) {
  OrderValidator.isValid(Request.all(), RULE_SAVE_ORDER);
  const data = Request.all();
  const repository = new OrderRepository();
  const result = await repository.create(data);
  res.json(ApiResponse.item(result, new OrderTransformer()));
}

async function update(req, res) {
  OrderValidator.isValid(Request.all(), RULE_UPDATE_ORDER);
  const repository = new OrderRepository();
  const result = await repository.update(Request.all(), req.params.id);
  res.json(ApiResponse.item(result, new OrderTransformer()));
}

async function destroy(req, res) {
  App.make(OrderRepository).deleteById(req.params.id);
  res.json(ApiResponse.success());
}

async function updateStatus(req, res) {
  OrderValidator.isValid(Request.all(), RULE_UPDATE_STATUS_ORDER);
  const id = req.params.id;
  const repository = new OrderRepository();
  const order = await repository.findById(id);
  if (!order) {
    throw new Exception('Order not found', 1001);
  }

  const status = Request.get('status');
  await order.update({ status: status });
  await order.reload();
  res.json(ApiResponse.item(order, new OrderTransformer()));
}

export default router;
