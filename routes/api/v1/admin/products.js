import express from 'express';
import ProductRepository from '../../../../app/Repositories/ProductRepository';
import ProductTransformer from '../../../../app/Transformers/ProductTransformer';
import { ProductValidator, RULE_SAVE_PRODUCT, RULE_UPDATE_STATUS_OF_PRODUCT } from '../../../../app/Validators/ProductValidator';
import { ApiResponse } from '@nsilly/response';
import { App } from '@nsilly/container';
import _ from 'lodash';
import { AsyncMiddleware, Request } from '@nsilly/support';
import CategoryRepository from '../../../../app/Repositories/CategoryRepository';
import { ImageValidator, RULE_CREATE } from '../../../../app/Validators/ImageValidator';
import Image from '../../../../app/Models/Image';
import Category from '../../../../app/Models/Category';
import { Exception } from '@nsilly/exceptions';
import { AuthMiddleware } from '@nsilly/auth';
import CategoryTransformer from '../../../../app/Transformers/CategoryTransformer';
import ImageTransformer from '../../../../app/Transformers/ImageTransformer';
import CategoryProductRepository from '../../../../app/Repositories/CategoryProductRepository';

const router = express.Router();

router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.put('/:id/status', AsyncMiddleware(changeStatus));
router.get('/:id/categories', AsyncMiddleware(getCategoryProduct));
router.get('/stock', AsyncMiddleware(getStockProduct));
router.get('/:id', AsyncMiddleware(show));
router.get('/sku/:sku', AsyncMiddleware(getSku));
router.get('/:id/images', AsyncMiddleware(getImage));
router.post('/list', AsyncMiddleware(list));
router.post('/', AsyncMiddleware(create));
router.post('/:id/images', AsyncMiddleware(addImage));
router.put('/:id', AsyncMiddleware(update));
router.put('/:id/categories', AsyncMiddleware(attachCategory));
router.delete('/:id', AsyncMiddleware(destroy));
router.put('/:id/stock', AsyncMiddleware(updateQuantity));
router.delete('/:id/images/:id_image', AsyncMiddleware(deleteImage));
router.delete('/:id/categories/:id_category', AsyncMiddleware(dettachCategory));
router.get('/:id/stock', AsyncMiddleware(getStockProductById));

async function list(req, res) {
  const category = Request.get('category');
  const repository = new ProductRepository();
  repository.applyOrderFromRequest();
  if (category) {
    repository.whereHas(Category, q => {
      q.where('slug', category);
      return q;
    });
  }
  const result = await repository.with(Image).get();
  res.json(ApiResponse.collection(result, new ProductTransformer(['images'])));
}

async function index(req, res) {
  const repository = new ProductRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest(['name', 'sku']);
  repository.applyOrderFromRequest();
  const result = await repository.paginate();
  res.json(ApiResponse.paginate(result, new ProductTransformer()));
}
async function show(req, res) {
  const product_id = req.params.id;
  const repository = new ProductRepository();

  const product = await repository
    .where('id', product_id)
    .with(Image)
    .with(Category)
    .first();

  res.json(ApiResponse.item(product, new ProductTransformer(['images', 'categories'])));
}

async function create(req, res) {
  ProductValidator.isValid(Request.all(), RULE_SAVE_PRODUCT);
  const data = Request.all();
  const repository = new ProductRepository();

  const images = Request.get('images');
  delete data['images'];
  const categories = Request.get('categories');
  delete data['categories'];
  const product = await repository.create(data);

  if (_.isArray(images)) {
    for (const item of images) {
      const data = { imageable_type: 'product', imageable_id: product.id, url: item.url, type: 'images' };
      await product.createImage(data);
    }
  }

  if (_.isArray(categories)) {
    for (const item of categories) {
      const category = await App.make(CategoryRepository).findById(item.id);
      await product.addCategories(category);
    }
  }

  const result = await App.make(ProductRepository)
    .where('id', product.id)
    .with(Image)
    .with(Category)
    .first();

  res.json(ApiResponse.item(result, new ProductTransformer(['images', 'categories'])));
}

async function update(req, res) {
  ProductValidator.isValid(Request.all(), RULE_UPDATE_STATUS_OF_PRODUCT);
  const product_id = req.params.id;
  const repository = new ProductRepository();

  const product = await repository.where('id', product_id).first();

  const imagesOfProduct = await product.getImages();

  if (Request.get('images')) {
    const images = Request.get('images');
    for (const item of images) {
      const data = _.assign({ ...item }, { type: 'images', imageable_type: 'product', imageable_id: product.id, url: item.url });
      await product.createImage(data);
    }
  }

  if (Request.get('categories')) {
    const categoriesOfProduct = await App.make(CategoryProductRepository)
      .where('product_id', product_id)
      .get();
    for (const cate of categoriesOfProduct) {
      await cate.destroy();
    }
    const categories = Request.get('categories');
    for (const item of categories) {
      if (item) {
        const category = await App.make(CategoryRepository).findById(item.id);
        await product.addCategory(category);
      }
    }
  }

  for (const item of imagesOfProduct) {
    await item.destroy();
  }

  await product.update(Request.all());
  await product.reload();

  res.json(ApiResponse.item(product, new ProductTransformer(['categories', 'images'])));
}

async function changeStatus(req, res) {
  ProductValidator.isValid(Request.all(), RULE_UPDATE_STATUS_OF_PRODUCT);
  const id = req.params.id;
  const repository = new ProductRepository();
  const product = await repository.findById(id);
  if (!product) {
    throw new Exception('Project not found', 1001);
  }

  const status = Request.get('status');
  await product.update({ status: status });
  res.json(ApiResponse.success());
}

async function destroy(req, res) {
  App.make(ProductRepository).deleteById(req.params.id);
  res.json(ApiResponse.success());
}

async function getSku(req, res) {
  const sku = req.params.sku;
  const repository = new ProductRepository();
  const result = await repository.where('sku', 'like', `%${sku}%`).get();
  res.json(ApiResponse.collection(result, new ProductTransformer()));
}

async function getCategoryProduct(req, res) {
  const product_id = req.params.id;
  const repository = new ProductRepository();
  const product = await repository.findById(product_id);

  const categories = await product.getCategories();

  res.json(ApiResponse.collection(categories, new CategoryTransformer()));
}

async function addImage(req, res) {
  const product_id = req.params.id;
  const repository = new ProductRepository();
  const images = Request.get('images');
  const product = await repository.where('id', product_id).first();

  const imagesOfProduct = await product.getImages();
  for (const item of images) {
    if (!_.find(imagesOfProduct, { url: item.url, imageable_type: 'product' })) {
      const data = _.assign({ ...item }, { imageable_type: 'product', imageable_id: product.id });
      ImageValidator.isValid(data, RULE_CREATE);
      await product.createImage(data);
    }
  }
  // await product.reload();

  // res.json(ApiResponse.item(product, new ProductTransformer(['images'])));
  res.json(ApiResponse.success());
}

async function getImage(req, res) {
  const id = req.params.id;
  const repository = new ProductRepository();
  const product = await repository.where('id', id).first();

  const images = await product.getImages();
  res.json(ApiResponse.collection(images, new ImageTransformer()));
}

async function deleteImage(req, res) {
  const product_id = req.params.id;
  const image_id = req.params.id_image;
  const repository = new ProductRepository();

  const product = await repository.findById(product_id);

  const images = await product.getImages({ where: { id: image_id, imageable_type: 'product' } });

  if (_.isEmpty(images)) {
    throw new Exception('Product has not been added image', 1001);
  }

  for (const image of images) {
    await image.destroy();
  }

  res.json(ApiResponse.success());
}

async function attachCategory(req, res) {
  const id = req.params.id;
  const category_id = Request.all().category_id;
  const repository = new ProductRepository();
  const product = await repository.findById(id);

  const categories = await product.getCategories();
  if (_.find(categories, ['id', parseInt(category_id)])) {
    throw new Exception('Product exitis Category', 1001);
  }

  const category = await App.make(CategoryRepository).findById(category_id);
  await product.addCategories(category);

  res.json(ApiResponse.success());
}

async function dettachCategory(req, res) {
  const product_id = req.params.id;
  const category_id = req.params.id_category;
  const repository = new ProductRepository();

  const product = await repository.findById(product_id);

  const categories = await product.getCategories();

  if (!_.find(categories, ['id', parseInt(category_id)])) {
    throw new Exception('Product not attach Category', 1001);
  }

  const category = await App.make(CategoryRepository).findById(category_id);
  await product.removeCategories(category);

  res.json(ApiResponse.success());
}

async function getStockProduct(req, res) {
  const repository = new ProductRepository();
  const product = await repository.withScope('getQuantity').paginate();
  res.json(ApiResponse.paginate(product, new ProductTransformer()));
}

async function updateQuantity(req, res) {
  const id = req.params.id;
  const usage = Request.get('quantity');
  const repository = new ProductRepository();
  const project = await repository.findById(id);

  const rest = project.quantity - usage;
  await project.update({ quantity: rest });
  await project.reload();
  res.json(ApiResponse.item(project, new ProductTransformer()));
}

async function getStockProductById(req, res) {
  const product_id = req.params.id;
  const repository = new ProductRepository();
  const product = await repository
    .where('id', product_id)
    .withScope('getQuantity')
    .first();
  res.json(ApiResponse.item(product, new ProductTransformer()));
}

export default router;
