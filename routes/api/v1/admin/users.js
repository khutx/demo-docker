import _ from 'lodash';
import express from 'express';
import UserRepository from '../../../../app/Repositories/UserRepository';
import UserTransformer from '../../../../app/Transformers/UserTransformer';
import RoleRepository from '../../../../app/Repositories/RoleRepository';
import { Hash } from '../../../../app/Services/Hash';
import CustomerRepository from '../../../../app/Repositories/CustomerRepository';
import { App } from '@nsilly/container';
import { NotFoundException, BadRequestHttpException, Exception } from '@nsilly/exceptions';
import { ApiResponse } from '@nsilly/response';
import { Request, AsyncMiddleware } from '@nsilly/support';
import { AuthMiddleware, Auth } from '@nsilly/auth';
import RoleUserRepository from '../../../../app/Repositories/RoleUserRepository';
import RoleTransformer from '../../../../app/Transformers/RoleTransformer';
import Role from '../../../../app/Models/Role';
import Customer from '../../../../app/Models/Customer';
import Recommendation, { NO_PENDING_UPDATE, PENDING_UPDATE } from '../../../../app/Models/Recommendation';
import ProductRepository from '../../../../app/Repositories/ProductRepository';
import Address from '../../../../app/Models/Address';
import { AddressValidator, RULE_SAVE_ADDRESS } from '../../../../app/Validators/AddressValidator';
import AddressRepository from '../../../../app/Repositories/AddressRepository';
import AddressTransformer from '../../../../app/Transformers/AddressTransformer';
import OrderTransformer from '../../../../app/Transformers/OrderTransformer';
import Product from '../../../../app/Models/Product';
import OrderRepository from '../../../../app/Repositories/OrderRepository';
import SubscriptionTransformer from '../../../../app/Transformers/SubscriptionTransformer';
import PlanRepository from '../../../../app/Repositories/PlanRepository';
import Subscription from '../../../../app/Models/Subscription';
import RecommendationTransformer from '../../../../app/Transformers/RecommendationTransformer';
import RecommendationRepository from '../../../../app/Repositories/RecommendationRepository';

const router = express.Router();

router.all('*', AuthMiddleware);

router.put('/:id/recommendation', AsyncMiddleware(updateProductRecommendation));
router.put('/:id/recommendation/:recommendation_id', AsyncMiddleware(updateRecommendation));
router.get('/:id/recommendation', AsyncMiddleware(getRecommendation));
router.post('/:id/recommendation', AsyncMiddleware(addRecommendation));
router.delete('/:id/recommendation/:product_id', AsyncMiddleware(deleteProductRecomendation));
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(store));
router.put('/me', AsyncMiddleware(updateMyProfile));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));
router.put('/:user_id/role', AsyncMiddleware(attachRole));
router.put('/:user_id/roles', AsyncMiddleware(saveUserRole));
router.delete('/:user_id/role/:role_id', AsyncMiddleware(dettachRole));
router.get('/:user_id/permissions', AsyncMiddleware(getPermissions));
router.get('/:user_id/roles', AsyncMiddleware(getRoles));
// router.post('/:id/customer', AsyncMiddleware(addCustomerProfile));
router.get('/:id/addresses', AsyncMiddleware(getAddressUser));
router.put('/:id/addresses/:address_id', AsyncMiddleware(updateAddressUser));
router.post('/:id/addresses', AsyncMiddleware(addAddressUser));
router.delete('/:id/addresses/:address_id', AsyncMiddleware(deleteAddressUser));
router.get('/:id/orders', AsyncMiddleware(getOrderUser));
router.get('/:id/subscription', AsyncMiddleware(getSubscriptionUser));
router.post('/:id/payment', AsyncMiddleware(AddCustomerStripe));
router.post('/:id/subscription', AsyncMiddleware(createSubscriptionStripe));
router.put('/:id/status', AsyncMiddleware(changeStatus));
router.put('/:id/subscription/:subscription_id', AsyncMiddleware(cancelSubscriptionStripe));
router.put('/:id/subscriptions/sync', AsyncMiddleware(syncSubscription));

async function deleteProductRecomendation(req, res) {
  const id = req.params.id;
  const product_id = req.params.product_id;
  const user = await App.make(UserRepository).findById(id);
  const recommendations = await user.getRecommendations();

  const recommendation = _.find(recommendations, ['product_id', parseInt(product_id)]);
  if (recommendation) {
    await recommendation.destroy();
  }
  res.json(ApiResponse.success());
}

const getUsersQuery = () => {
  const repository = new UserRepository();
  const transformer = new UserTransformer();

  let is_filtering_by_role = false;

  repository.applySearchFromRequest(['email']);

  repository.applyConstraintsFromRequest({
    role_ids: roles => {
      is_filtering_by_role = true;
      repository.whereHas(Role, q => {
        q.whereIn('id', roles.split(','));
        return q;
      });
    }
  });

  if (Request.has('roles')) {
    const roles = Request.get('roles').split(',');
    is_filtering_by_role = true;
    repository.whereHas(Role, function(q) {
      q.whereIn('slug', roles);
      return q;
    });
  }

  if (Request.has('sort') && Request.get('sort') !== '') {
    const orderBy = Request.get('sort').split(',');
    orderBy.forEach(field => {
      let direction = 'ASC';
      if (field.charAt(0) === '-') {
        direction = 'DESC';
        field = field.slice(1);
      }
      if (field.charAt(0) === '+') {
        field = field.slice(1);
      }
      if (field === 'customer_name') {
        repository.orderBy(Customer, 'first_name', direction);
      }
      if (field === 'customer_group') {
        repository.orderBy(Customer, 'group', direction);
      }
      if (field === 'customer_phone') {
        repository.orderBy(Customer, 'phone', direction);
      }
    });
  }

  if (Request.has('ids')) {
    repository.whereIn('id', Request.get('ids'));
  }

  if (Request.has('includes')) {
    const includes = _.split(Request.get('includes'), ',');

    if (_.includes(includes, 'customer')) {
      repository.withScope('customer');
      transformer.with('customer');
    }

    if (_.includes(includes, 'roles')) {
      if (!is_filtering_by_role) {
        repository.with(Role);
      }
      transformer.with('roles');
    }
  }
  return { query: repository, transformer };
};

async function index(req, res) {
  const { query, transformer } = getUsersQuery();
  const result = await query.paginate();
  res.json(ApiResponse.paginate(result, transformer));
}

async function updateProductRecommendation(req, res) {
  const id = req.params.id;
  const repository = new UserRepository();
  const products = Request.all().products;
  const user = await repository.findById(id);

  const recommendations = await user.getRecommendations();

  for (const item of products) {
    const product_id = item.id;
    delete item.id;
    const data = _.assign({ ...item }, { product_id: product_id });

    if (!_.find(recommendations, { product_id: product_id })) {
      await user.createRecommendation(data);
    }

    const listFind = _.remove(recommendations, n => {
      return n.product_id === product_id;
    });
    for (const find of listFind) {
      await find.update(data);
    }
  }

  for (const item of recommendations) {
    await item.destroy();
  }
  await user.reload();
  const result = await user.getRecommendations();
  res.json(ApiResponse.collection(result, new RecommendationTransformer()));
}

async function updateRecommendation(req, res) {
  const id = req.params.id;
  const recommendation_id = req.params.recommendation_id;
  const data = Request.all();
  const repository = new UserRepository();
  const user = await repository.where('id', id).first();

  if (!user) {
    throw new Exception(`User ${id} not found`, 1001);
  }

  const recommendations = await user.getRecommendations();
  const recommendation = _.find(recommendations, { id: parseInt(recommendation_id) });

  if (!recommendation) {
    throw new Exception('User not exitis Recommendation', 1001);
  }

  const quantityDB = recommendation.quantity;
  const quantityRequest = data.quantity;
  if (quantityDB !== quantityRequest) {
    await recommendation.update({
      quantity: data.quantity,
      note: data.note,
      status: PENDING_UPDATE
    });
  }

  await recommendation.update(data);

  res.json(ApiResponse.item(recommendation, new RecommendationTransformer()));
}

async function getRecommendation(req, res) {
  const id = req.params.id;
  const repository = new UserRepository();
  const user = await repository.findById(id);
  if (!user) {
    throw new Exception(`User ${id} not found`, 1001);
  }

  const recommendations = await App.make(RecommendationRepository)
    .where('user_id', id)
    .with(Product)
    .get();

  for (const item of recommendations) {
    if (!_.isNil(item.product)) {
      item.product.images = await item.product.getImages();
    }
  }

  res.json(ApiResponse.collection(recommendations, new RecommendationTransformer(['product'])));
}

async function addRecommendation(req, res) {
  const id = req.params.id;
  const data = Request.all();
  const repository = new UserRepository();
  const user = await repository
    .where('id', id)
    .with(Recommendation)
    .first();

  if (!user) {
    throw new Exception(`User ${id} not found`, 1001);
  }

  const recommendation = _.find(user.recommendations, { product_id: parseInt(data.product_id) });
  if (!recommendation) {
    await App.make(ProductRepository).findById(data.product_id);
    await user.createRecommendation(data);
  }

  await user.reload();
  res.json(ApiResponse.collection(user.recommendations, new RecommendationTransformer(['product'])));
}

async function show(req, res) {
  const userId = req.params.id;
  const repository = new UserRepository();
  const user = await repository
    .where('id', userId)
    .with(Role)
    .with(Customer)
    .first();

  if (!user) {
    throw new NotFoundException('User');
  }

  res.json(ApiResponse.item(user, new UserTransformer(['roles', 'customer'])));
}

async function store(req, res) {
  if (Request.get('password') !== Request.get('re_password')) {
    throw new Error('Password does not match', 1000);
  }

  //   UserValidator.isValid(Request.all(), CREATE_USER_RULE);

  const role = await App.make(RoleRepository).findById(Request.get('role_id'));

  const repository = new UserRepository();

  const data = {
    email: Request.get('email'),
    password: App.make(Hash).hash(Request.get('password'))
  };

  const user = await repository.create(data);

  await user.addRole(role);

  if (role.isCustomer()) {
    const data = {
      first_name: Request.get('first_name'),
      last_name: Request.get('last_name'),
      birth: Request.get('birth'),
      gender: Request.get('gender'),
      phone: Request.get('phone'),
      user_id: user.id
    };
    await App.make(CustomerRepository).create(data);
  }

  const result = App.make(UserRepository)
    .with(Customer)
    .with(Role);
  res.json(ApiResponse.item(result, new UserTransformer(['customer', 'roles'])));
}

async function update(req, res) {
  const user_id = req.params.id;
  const repository = new UserRepository();
  const user = await repository.findById(user_id);
  if (Request.has('email') && Request.get('email') !== user.email) {
    const isExistEmail =
      (await repository
        .where('email', Request.get('email'))
        .where('id', '<>', user_id)
        .count()) > 0;
    if (isExistEmail) {
      throw new BadRequestHttpException('Email already exist', 1000);
    }
  }

  res.json(ApiResponse.item(user, new UserTransformer()));
}

async function updateMyProfile(req, res) {
  const id = Auth.user().id;
  const changes = { updated_at: new Date() };

  if (req.body.email) changes['email'] = req.body.email;

  const user = await updateUserFn(id, changes);

  res.json(ApiResponse.item(user, new UserTransformer()));
}

async function updateUserFn(id, data) {
  let user = null;
  if (!data.email) {
    user = await App.make(UserRepository).updateOrCreate(data, { id });
  } else {
    user = await App.make(UserRepository).findById(id);
    if (user.email !== data.email) {
      const existing_user = await App.make(UserRepository)
        .where('email', data.email)
        .first();

      if (existing_user) {
        throw new Exception('Email already exists', 1000);
      }

      delete data.email;

      await App.make(UserRepository).updateOrCreate(data, { id });
    } else {
      await App.make(UserRepository).updateOrCreate(data, { id });
    }
  }

  return user;
}

async function destroy(req, res) {
  const id = req.params.id;
  const repository = new UserRepository();
  const user = await repository.findById(id);
  if (!user) {
    throw new Exception('User not found', 1000);
  }
  await user.destroy();
  const item = await App.make(CustomerRepository)
    .where('user_id', id)
    .first();
  await item.destroy();
  res.json(ApiResponse.success());
}

async function attachRole(req, res) {
  const user_id = req.params.user_id;
  const role_id = req.body.role_id;

  const role = await App.make(RoleRepository).findById(role_id);
  const user = await App.make(UserRepository).findById(user_id);

  await user.addRole(role);

  res.json(ApiResponse.success());
}

/**
 * Erase all roles of user and attach new roles to user back
 *
 * @param req http request
 * @param res http response
 *
 * @return ApiResponse
 */
async function saveUserRole(req, res) {
  const userId = req.body.user_id;
  const roleIds = req.body.role_ids;
  const roles = await App.make(RoleRepository)
    .whereIn('id', roleIds)
    .get();
  const user = await App.make(UserRepository).findById(userId);

  await App.make(RoleUserRepository)
    .where('user_id', userId)
    .delete();

  for (const role of roles) {
    await user.addRole(role);
  }

  res.json(ApiResponse.success());
}

/**
 * Dettach role from user
 *
 *
 * @return ApiResponse.success
 */

async function dettachRole(req, res) {
  const user_id = req.params.user_id;
  const role_id = req.params.role_id;

  const repository = new RoleUserRepository();

  const role_user = await repository
    .where('user_id', user_id)
    .where('role_id', role_id)
    .first();

  if (!role_user) {
    throw new Exception('Role was not attached to user', 1000);
  }

  await repository
    .where('user_id', user_id)
    .where('role_id', role_id)
    .delete();

  res.json(ApiResponse.success());
}

async function getPermissions(req, res) {
  const user_id = req.params.user_id;
  const repository = new UserRepository();
  let permissions = [];
  const user = await repository.with(Role).findById(user_id);

  _.forEach(user.roles, item => {
    permissions = permissions.concat(JSON.parse(item.permissions));
  });

  res.json(ApiResponse.array(permissions));
}

/**
 * Get all role of a user
 *
 * @return ApiResponse.array
 */
async function getRoles(req, res) {
  const user_id = req.params.user_id;
  const roles = await App.make(RoleUserRepository)
    .orderBy('id', 'DESC')
    .where('user_id', user_id)
    .get();
  res.json(ApiResponse.collection(roles, new RoleTransformer()));
}

/**
 * add customer data to user profile
 *
 * @return ApiResponse.item
 */

// async function addCustomerProfile(req, res) {
//   const user_id = req.params.id;
//   const data = {
//     user_id: user_id,
//     first_name: Request.get('first_name'),
//     last_name: Request.get('last_name'),
//     birth: Request.get('birth'),
//     gender: Request.get('gender'),
//     phone: Request.get('phone'),
//     group: ''
//   };

//   const repository = new UserRepository();

//   const user = await repository
//     .where('id', user_id)
//     .with('customer')
//     .first();

//   if (!user) {
//     throw new NotFoundException('User');
//   }

//   if (!_.isNil(user.get('customer'))) {
//     throw new Exception('Customer data already added', 1000);
//   }

//   const customer = await App.make(CustomerRepository).create(data);
//   const customerForHook = await new CustomerRepository().getHookData(customer.id);
//   await new MagentoService().invokeMagentoWebhook('customer', customerForHook);

//   if (_.isNil(customer)) {
//     throw new Exception('Can not add data', 1000);
//   }
//   await user.reload();

//   res.json(ApiResponse.item(user, new UserTransformer(['customer'])));
// }

/**
 * Update customer data of user
 *
 *
 * @return ApiResponse.item
 */

// async function updateCustomerUser(req, res) {
//   const user_id = req.params.id;
//   const customer_update = {
//     user_id: req.params.id,
//     email: req.body.email,
//     first_name: req.body.first_name,
//     last_name: req.body.last_name,
//     birth: req.body.birth,
//     gender: req.body.gender,
//     phone: req.body.phone,
//     group: req.body.group,
//     updated_at: new Date()
//   };

//   _.mapKeys(customer_update, (value, key) => {
//     if (_.isNil(customer_update[key])) {
//       delete customer_update[key];
//     }
//   });
//   const repository = new UserRepository();
//   const user = await repository
//     .where('id', user_id)
//     .with('customer')
//     .first();

//   if (!user) {
//     throw new Exception('User not found', 1000);
//   }

//   if (!user.customer) {
//     throw new Exception('Customer data was not added to user', 1000);
//   }
//   const result = await user.customer.update(customer_update);

//   const customerForHook = await new CustomerRepository().getHookData(result.id);
//   await new MagentoService().invokeMagentoWebhook('customer', customerForHook);

//   if (!result) {
//     throw new Exception('Can not update customer data', 1000);
//   }

//   await user.reload();

//   res.json(ApiResponse.item(user, new UserTransformer(['customer'])));
// }

async function getAddressUser(req, res) {
  const user_id = req.params.id;
  await App.make(UserRepository).findById(user_id);
  const addresses = await App.make(AddressRepository)
    .where('user_id', user_id)
    .get();
  res.json(ApiResponse.collection(addresses, new AddressTransformer()));
}

async function updateAddressUser(req, res) {
  const user_id = req.params.id;
  const address_id = req.params.address_id;
  const data = Request.all();
  AddressValidator.isValid(data, RULE_SAVE_ADDRESS);
  const repository = new UserRepository();
  const user = await repository
    .where('id', user_id)
    .with(Address)
    .first();

  if (!user) {
    throw new Exception('User not found !', 1001);
  }

  const addresses = await user.getAddresses();
  const address = _.find(addresses, { id: parseInt(address_id) });
  if (!address) {
    throw new Exception('Address is not exist in User !', 1001);
  }
  const result = await address.update(data);

  res.json(ApiResponse.item(result, new AddressTransformer()));
}
async function addAddressUser(req, res) {
  const user_id = req.params.id;
  const data = Request.all();
  const repository = new UserRepository();
  const user = await repository
    .where('id', user_id)
    .with(Address)
    .first();

  if (!user) {
    throw new Exception('User not found', 1001);
  }

  await user.createAddress(data);

  await user.reload();
  res.json(ApiResponse.item(user, new UserTransformer(['addresses'])));
}

async function deleteAddressUser(req, res) {
  const user_id = req.params.id;
  const address_id = req.params.address_id;
  const repository = new UserRepository();
  const user = await repository
    .where('id', user_id)
    .with(Address)
    .first();

  if (!user) {
    throw new Exception('User not found !', 1001);
  }

  const addresses = await user.getAddresses();
  const address = _.find(addresses, { id: parseInt(address_id) });
  if (!address) {
    throw new Exception('Address is not exist in User !', 1001);
  }
  await address.destroy();
  res.json(ApiResponse.success());
}

async function getOrderUser(req, res) {
  const user_id = req.params.id;
  const repository = new UserRepository();
  await repository.findById(user_id);
  const orders = await App.make(OrderRepository)
    .where('user_id', user_id)
    .with(Product)
    .paginate();

  res.json(ApiResponse.paginate(orders, new OrderTransformer(['products'])));
}

async function getSubscriptionUser(req, res) {
  const id = req.params.id;
  const reposirory = new UserRepository();
  const user = await reposirory
    .where('id', id)
    .withScope('getSubscription')
    .first();

  if (!user.subscription) {
    res.json(ApiResponse.item(user.subscription, new SubscriptionTransformer()));
  } else {
    res.json(ApiResponse.item(user.subscription, new SubscriptionTransformer(['plan'])));
  }
}

async function AddCustomerStripe(req, res) {
  const id = req.params.id;
  const card_token = Request.get('card_token');
  const repository = new UserRepository();
  const user = await repository.findById(id);
  const email = user.email;
  const customerOfUser = await user.getCustomer();
  const customer_stripe = await App.make('PaymentGateway').createCustomer({
    description: `Customer for ${email}`,
    source: card_token
  });
  const source = await App.make('PaymentGateway').getSourceCustomer(customer_stripe.id);

  await customerOfUser.update({
    payment_id: customer_stripe.id,
    card_last_four: source.last4,
    card_brand: source.brand
  });
  res.json(ApiResponse.success());
}

async function createSubscriptionStripe(req, res) {
  const id = req.params.id;
  const repository = new UserRepository();
  const user = await repository
    .where('id', id)
    .with(Recommendation)
    .with(Customer)
    .with(Subscription)
    .first();
  const email = user.email;
  const recommentdationOfUser = await user.getRecommendations();
  const customer = await user.getCustomer();
  const checkRecommendation = _.isEmpty(recommentdationOfUser);
  if (!checkRecommendation) {
    const listAmount = [];
    for (let i = 0; i < recommentdationOfUser.length; i++) {
      const quantity = recommentdationOfUser[i].quantity;
      const product = await recommentdationOfUser[i].getProduct();
      const price = product.price;
      const amount = quantity * price;
      listAmount.push(amount);
      var total = _.sum(listAmount);
    }

    const planStripe = await App.make('PaymentGateway').createPlan({
      amount: total,
      interval: 'month',
      product: {
        name: `Product for ${email}`
      },
      currency: process.env.CURRENCY,
      nickname: `Plan of ${email} `
    });

    const plan = await App.make(PlanRepository).create({
      nickname: planStripe.nickname,
      plan_id: planStripe.id,
      interval: planStripe.interval,
      amount: planStripe.amount,
      currency: planStripe.currency
    });
    const subscription = await App.make('PaymentGateway').subscribe({
      customer_id: customer.payment_id,
      plan: planStripe.id
    });

    const subscriptionOfUser = await user.getSubscription();
    if (subscriptionOfUser) {
      await subscriptionOfUser.update({
        subscription_id: subscription.id,
        status: subscription.status,
        plan_id: plan.id
      });
    } else {
      await user.createSubscription({
        subscription_id: subscription.id,
        status: subscription.status,
        plan_id: plan.id,
        start_time: new Date(subscription.created * 1000)
      });
    }
    res.json(ApiResponse.success());
  } else {
    throw new Exception('Recommendation not exist product !', 1001);
  }
}

async function changeStatus(req, res) {
  const id = req.params.id;
  const repository = new UserRepository();
  const user = await repository.findById(id);
  if (!user) {
    throw new Exception('User not found !', 1001);
  }

  const status = Request.get('status');
  await user.update({ status: status });
  await user.reload();
  res.json(ApiResponse.item(user, new UserTransformer()));
}

async function cancelSubscriptionStripe(req, res) {
  const id = req.params.id;
  const id_subscription = req.params.subscription_id;
  const repository = new UserRepository();
  const user = await repository.findById(id);

  const subscriptionOfUser = await user.getSubscription();
  const subscription = _.find(subscriptionOfUser, { id: parseInt(id_subscription) });
  const customer = await user.getCustomer();
  if (subscription.status === 'active') {
    const subCancel = await App.make('PaymentGateway').cancelSubscription(subscription.subscription_id);
    await subscriptionOfUser.update({ status: subCancel.status });
  } else {
    const planDB = await subscriptionOfUser.getPlan();
    const sub = await App.make('PaymentGateway').subscribe({
      customer_id: customer.payment_id,
      plan: planDB.plan_id
    });
    await subscriptionOfUser.update({
      subscription_id: sub.id,
      status: sub.status
    });
  }
  res.json(ApiResponse.success());
}

async function syncSubscription(req, res) {
  const id = req.params.id;
  const user = await App.make(UserRepository).findById(id);
  const email = user.email;
  const recommentdationOfUser = await user.getRecommendations();
  const subscriptionOfUser = await user.getSubscription();
  const listAmount = [];
  for (let i = 0; i < recommentdationOfUser.length; i++) {
    const quantity = recommentdationOfUser[i].quantity;
    const product = await recommentdationOfUser[i].getProduct();
    const price = product.price;
    const amount = quantity * price;
    listAmount.push(amount);
    var total = _.sum(listAmount);
  }

  const planStripe = await App.make('PaymentGateway').createPlan({
    amount: total,
    interval: 'month',
    product: {
      name: `Product for ${email}`
    },
    currency: process.env.CURRENCY,
    nickname: `Plan of ${email} `
  });
  const plan = await subscriptionOfUser.getPlan();
  await plan.update({ amount: parseFloat(total), plan_id: planStripe.id });
  await App.make('PaymentGateway').updateSubscription(subscriptionOfUser.subscription_id, {
    plan: planStripe.id
  });
  const recommentdation = _.filter(recommentdationOfUser, { status: PENDING_UPDATE });
  for (let i = 0; i < recommentdation.length; i++) {
    await recommentdation[i].update({ status: NO_PENDING_UPDATE });
  }
  res.json(ApiResponse.success());
}

module.exports = router;
