import express from 'express';
import EmailRepository from '../../../app/Repositories/EmailRepository';
import EmailTransformer from '../../../app/Transformers/EmailTransformer';
import { EmailValidator, RULE_SAVE_EMAIL } from '../../../app/Validators/EmailValidator';
import { App } from '@nsilly/container';
import { ApiResponse } from '@nsilly/response';
import { AsyncMiddleware, Request } from '@nsilly/support';
// import { AuthMiddleware } from '../../../app/Middlewares/AuthMiddleware';

const router = express.Router();

// router.all('*', AuthMiddleware);
router.get('/', AsyncMiddleware(index));
router.get('/:id', AsyncMiddleware(show));
router.post('/', AsyncMiddleware(create));
router.put('/:id', AsyncMiddleware(update));
router.delete('/:id', AsyncMiddleware(destroy));

async function index(req, res) {
  const repository = new EmailRepository();
  repository.applyConstraintsFromRequest();
  repository.applySearchFromRequest([]);
  repository.applyOrderFromRequest();
  const email = await repository.paginate();
  res.json(ApiResponse.paginate(email, new EmailTransformer()));
}

async function show(req, res) {
  const email = await App.make(EmailRepository).findById(req.params.id);
  res.json(ApiResponse.item(email, new EmailTransformer()));
}

async function create(req, res) {
  EmailValidator.isValid(Request.all(), RULE_SAVE_EMAIL);
  const email = await App.make(EmailRepository).create(Request.all());
  res.json(ApiResponse.item(email, new EmailTransformer()));
}

async function update(req, res) {
  EmailValidator.isValid(Request.all(), RULE_SAVE_EMAIL);
  const email_id = req.params.id;
  const email = await App.make(EmailRepository).findById(email_id);
  await email.update(Request.all());
  res.json(ApiResponse.success());
}

async function destroy(req, res) {
  const email_id = req.params.id;
  const email = await App.make(EmailRepository).findById(email_id);
  await email.destroy();
  res.json(ApiResponse.success());
}

export default router;
